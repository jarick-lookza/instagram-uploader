<?php

namespace Exception;

/**
 * Class PhotoFileIsNotFound
 * @package Exception
 */
class PhotoFileIsNotFound extends Exception
{
    /**
     * LoginFailed constructor.
     */
    public function __construct(string $path)
    {
        parent::__construct("Photo file `$path` not found", 0, null);
    }
}
