<?php

namespace Exception;

/**
 * Class SessionWriteError
 * @package Exception
 */
class SessionWriteError extends Exception
{
    /**
     * SessionWriteError constructor.
     */
    public function __construct()
    {
        parent::__construct('Failed save session', 0, null);
    }
}