<?php

namespace Exception;

/**
 * Class UploadError
 * @package Exception
 */
class UploadError extends Exception
{
    /**
     * UploadError constructor.
     */
    public function __construct()
    {
        parent::__construct('Failed load photo to instagram', 0, null);
    }
}
