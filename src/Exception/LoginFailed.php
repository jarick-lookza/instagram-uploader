<?php

namespace Exception;

/**
 * Class LoginFailed
 * @package Exception
 */
class LoginFailed extends Exception
{
    /**
     * LoginFailed constructor.
     */
    public function __construct()
    {
        parent::__construct('Failed login to instagram', 0, null);
    }
}
