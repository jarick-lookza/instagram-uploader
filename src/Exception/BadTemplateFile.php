<?php

namespace Exception;

/**
 * Class BadTemplateFile
 * @package Exception
 */
class BadTemplateFile extends Exception
{
    /**
     * BadTemplateFile constructor.
     */
    public function __construct()
    {
        parent::__construct('Template deserialize failed', 0, null);
    }
}
