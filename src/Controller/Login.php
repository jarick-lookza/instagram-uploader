<?php

namespace Controller;

use Config;
use Service\Auth;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Slim\Views\PhpRenderer;
use Validator\LoginValidator;

/**
 * Class Login
 * @package Controller
 */
class Login extends Base
{
    /**
     * @var string
     */
    public static $PATH = '/login';

    /**
     * @var string
     */
    public static $PATH_LOGOUT = '/logout';

    /**
     * @var string
     */
    public static $USERNAME = 'username';

    /**
     * @var string
     */
    public static $PASSWORD = 'password';

    /**
     * @var string
     */
    public static $TEMPLATE = 'login.phtml';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var PhpRenderer
     */
    protected $renderer;

    /**
     * @var Auth
     */
    protected $auth;

    /**
     * @var LoginValidator
     */
    protected $validator;

    /**
     * Login constructor.
     * @param Config $config
     * @param PhpRenderer $renderer
     * @param Auth $auth
     * @param LoginValidator $validator
     */
    public function __construct(
        Config $config,
        PhpRenderer $renderer,
        Auth $auth,
        LoginValidator $validator
    )
    {
        $this->config = $config;
        $this->renderer = $renderer;
        $this->auth = $auth;
        $this->validator = $validator;
    }

    /**
     * Index route.
     * @param Request $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface|Response
     */
    public function index(
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        return $this->renderer->render($response, self::$TEMPLATE);
    }

    /**
     * Save route.
     * @param Request $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface|Response
     */
    public function save(Request $request, Response $response)
    {
        $validator = $this->validator->withData($request->getParams());

        if (!$validator->validate()) {
            return $response
                ->withStatus(StatusCode::HTTP_BAD_REQUEST)
                ->withJson($validator->errors());
        }

        $username = $request->getParam(self::$USERNAME);
        $password = $request->getParam(self::$PASSWORD);

        if ($this->auth->login($username, $password)) {
            return $response->withStatus(StatusCode::HTTP_ACCEPTED);
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

    /**
     * Logout route.
     * @param Request $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface|Response
     */
    public function logout(
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        $this->auth->logout();

        return $this->redirect($response, Upload::$PATH);
    }
}
