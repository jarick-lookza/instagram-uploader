<?php

namespace Controller;

use Config;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Slim\Views\PhpRenderer;
use Slim\Http\UploadedFile;

class Upload
{
    /**
     * @var string
     */
    public static $PATH = '/';

    /**
     * @var string
     */
    public static $TEMPLATE = 'index.phtml';

    protected $config;
    protected $renderer;
    protected $moveUploadedFile;

    public function __construct(
        Config $config,
        PhpRenderer $renderer,
        callable $moveUploadedFile
    )
    {
        $this->config = $config;
        $this->renderer = $renderer;
        $this->moveUploadedFile = $moveUploadedFile;
    }

    /**
     * Index route.
     * @param Request $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface|Response
     */
    public function index(
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        return $this->renderer->render($response, self::$TEMPLATE);
    }

    /**
     * Save uploaded files in upload folder.
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function save(Request $request, Response $response)
    {
        $directory = $this->config->getUploadFolder();
        $uploadedFiles = $request->getUploadedFiles();

        /** @var UploadedFile $uploadedFile */
        foreach ($uploadedFiles as $uploadedFile) {
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                call_user_func($this->moveUploadedFile, $directory, $uploadedFile);
            }
        }

        return $response->withStatus(StatusCode::HTTP_CREATED);
    }
}
