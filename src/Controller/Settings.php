<?php

namespace Controller;

use Entity\Template;
use Repository\InstagramApi;
use Repository\TemplateStorage;
use Serializer\Json;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Slim\Views\PhpRenderer;
use Config;
use Socket\Client;
use Validator\TemplateValidator;

/**
 * Class Settings
 * @package Controller
 */
class Settings extends Base
{
    /**
     * @var string
     */
    public static $PATH = '/settings';

    /**
     * @var string
     */
    public static $PATH_LOCATION = '/settings/locations';

    /**
     * @var string
     */
    public static $PATH_START_SCRIPT = '/settings/start';

    /**
     * @var string
     */
    public static $PATH_STOP_SCRIPT = '/settings/stop';

    /**
     * @var string
     */
    public static $TEMPLATE = 'settings.phtml';

    /**
     * @var PhpRenderer
     */
    protected $renderer;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var TemplateStorage
     */
    protected $templateStorage;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * @var InstagramApi
     */
    protected $api;

    /**
     * @var TemplateValidator
     */
    protected $validator;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Settings constructor.
     * @param Config $config
     * @param PhpRenderer $renderer
     * @param TemplateStorage $templateStorage
     * @param Client $client
     * @param Json $serializer
     * @param InstagramApi $api
     * @param TemplateValidator $validator
     */
    public function __construct(
        Config $config,
        PhpRenderer $renderer,
        TemplateStorage $templateStorage,
        Client $client,
        Json $serializer,
        InstagramApi $api,
        TemplateValidator $validator
    )
    {
        $this->renderer = $renderer;
        $this->config = $config;
        $this->templateStorage = $templateStorage;
        $this->serializer = $serializer;
        $this->api = $api;
        $this->validator = $validator;
        $this->client = $client;
    }

    /**
     * Render settings page.
     * @param Request $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception\BadTemplateFile
     */
    public function get(
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        $template = $this->templateStorage->get();
        $auth = $this->api->geAuthByTemplate($template);
        $location = $template->getLocation();
        $locationName = '';
        if ($location !== '') {
            $json = $this->serializer->decode($template->getLocation());
            $locationName = isset($json['name']) ? $json['name'] : '';
        }
        $text = $template->getCaption();
        $period = $template->getPeriod();

        try {
            $uploadScriptIsActive = $this->client->isActive();
            $uploadScriptIsRunning = true;
        } catch (\Exception $e) {
            $uploadScriptIsActive = false;
            $uploadScriptIsRunning = false;
        }

        return $this->renderer->render($response, self::$TEMPLATE, [
            'instagram_login' => $auth->getLogin(),
            'instagram_password' => $auth->getPassword(),
            'location' => $location,
            'location_name' => $locationName,
            'text' => $text,
            'period' => $period,
            'upload_script_is_active' => $uploadScriptIsActive,
            'upload_script_is_running' => $uploadScriptIsRunning,
        ]);
    }

    /**
     * Search location by query.
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception\BadTemplateFile
     * @throws \Exception\LoginFailed
     */
    public function search(Request $request, Response $response)
    {
        $search = $request->getQueryParam('search', '');

        if ($search === '') {
            return $response->withJson([]);
        }

        $template = $this->templateStorage->get();
        $this->api->login($template);
        $result = iterator_to_array($this->api->search($search));

        return $response->withJson($result);
    }

    /**
     * Set settings params.
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function save(Request $request, Response $response)
    {
        $validator = $this->validator->withData($request->getParams());

        if (!$validator->validate()) {
            return $response
                ->withStatus(StatusCode::HTTP_BAD_REQUEST)
                ->withJson($validator->errors());
        }

        $instagramLogin = $request->getParam('instagram_login');
        $instagramPassword = $request->getParam('instagram_password');
        $location = $request->getParam('location', '');
        $text = $request->getParam('text');
        $period = (int) $request->getParam('period');

        $template = new Template($this->config, $text, $location, [
            'instagram_login' => $instagramLogin,
            'instagram_password' => $instagramPassword,
            'period' => $period,
        ]);

        $this->templateStorage->save($template);
        try {
            if ($this->client->isActive()) {
                $this->client->restart();
            }
        } catch (\Exception $e) {}

        return $response->withStatus(StatusCode::HTTP_ACCEPTED);
    }

    /**
     * Start upload photo script.
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function startScript(
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        $this->client->start();

        return $response->withStatus(StatusCode::HTTP_ACCEPTED);
    }

    /**
     * Stop upload photo script.
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function stopScript
    (
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        $this->client->stop();

        return $response->withStatus(StatusCode::HTTP_ACCEPTED);
    }
}
