<?php

namespace Controller;

use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class Base
 * @package Controller
 */
abstract class Base {
    /**
     * @var string
     */
    public static $LOCATION = 'Location';

    /**
     * Send redirect.
     * @param Response $response
     * @param string $path
     * @return Response
     */
    public function redirect(Response $response, string $path)
    {
        return $response
            ->withStatus(StatusCode::HTTP_FOUND)
            ->withHeader(self::$LOCATION, $path);
    }
}