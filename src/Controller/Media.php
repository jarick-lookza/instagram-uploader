<?php

namespace Controller;

use Entity\File;
use Exception\PhotoFileIsNotFound;
use League\Flysystem\Filesystem;
use Repository\FilesStorage;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Slim\Views\PhpRenderer;

class Media
{
    /**
     * @var string
     */
    public static $PATH = '/media';

    /**
     * @var string
     */
    public static $PATH_GALLERY = '/media/gallery';

    /**
     * @var string
     */
    public static $PATH_DELETE = '/media/delete';

    /**
     * @var string
     */
    public static $PATH_STATS = '/media/stats';

    /**
     * @var string
     */
    public static $TEMPLATE = 'media.phtml';

    /**
     * @var int
     */
    public static $MAX_PAGINATE = 100;

    /**
     * @var int
     */
    public static $DEFAULT_PAGINATE = 50;

    /**
     * @var PhpRenderer
     */
    protected $renderer;

    /**
     * @var FilesStorage
     */
    protected $files;

    /**
     * Media constructor.
     * @param PhpRenderer $renderer
     * @param FilesStorage $files
     * @param Filesystem $fileSystem
     */
    public function __construct(
        PhpRenderer $renderer,
        FilesStorage $files
    )
    {
        $this->files = $files;
        $this->renderer = $renderer;
    }

    /**
     * Index route.
     * @param Request $request
     * @param Response $response
     * @return \Psr\Http\Message\ResponseInterface|Response
     */
    public function index(
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        $size = $this->files->getPhotosSize();
        $count = $this->files->getPhotosCount();

        return $this->renderer->render($response, self::$TEMPLATE, [
            'size' => $size,
            'count' => $count,
        ]);
    }

    /**
     * Filter files by file name with limits.
     * @param string $cursor
     * @param int $limit
     * @return \Generator|void
     */
    private function filterFilesByCursorAndLimit(string $cursor, int $limit)
    {
        $counter = 0;
        $find = $cursor === '';

        /** @var File $file */
        foreach ($this->files->getAll() as $file) {
            if (!$find) {
                if ($file->getName() === $cursor) {
                    $find = true;
                    continue;
                }
            }

            if ($find) {
                if ($counter >= $limit) {
                    return;
                }

                yield $file->getName();
                $counter++;
            }
        }
    }

    /**
     * Return json with list of name files.
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getGallery(Request $request, Response $response)
    {
        $cursor = $request->getQueryParam('cursor', '');
        $limit = (int) $request->getQueryParam('limit', self::$DEFAULT_PAGINATE);
        $limit = max(1, $limit);
        $limit = min(self::$MAX_PAGINATE, $limit);

        $list = iterator_to_array($this->filterFilesByCursorAndLimit($cursor, $limit));

        return $response->withJson($list);
    }

    /**
     * Delete file by name.
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function delete(Request $request, Response $response)
    {
        $name = $request->getParam('name', '');

        if ($name === '') {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        try {
            $this->files->delete($name);

            return $response->withStatus(StatusCode::HTTP_ACCEPTED);
        } catch (PhotoFileIsNotFound $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }
    }

    /**
     * Return statistic about uploaded files.
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getStats(
        /** @noinspection PhpUnusedParameterInspection */ Request $request,
                                                          Response $response
    )
    {
        $size = $this->files->getPhotosSize();
        $count = $this->files->getPhotosCount();

        return $response->withJson([
            'size' => $size,
            'count' => $count,
        ]);
    }
}
