<?php

namespace Service;

use Repository\FilesStorage;
use Repository\InstagramApi;
use Repository\SessionStorage;
use Repository\TemplateStorage;

/**
 * Class InstagramUploader
 * @package Service
 */
class InstagramUploader
{
    /**
     * @var SessionStorage
     */
    protected $sessionStorage;

    /**
     * @var FilesStorage
     */
    protected $fileStorage;

    /**
     * @var TemplateStorage
     */
    protected $templateStorage;

    /**
     * @var callable
     */
    protected $createInstagramPhoto;

    /**
     * @var InstagramApi
     */
    protected $api;

    /**
     * InstagramUploader constructor.
     * @param SessionStorage $sessionStorage
     * @param FilesStorage $fileStorage
     * @param TemplateStorage $templateStorage
     * @param InstagramApi $api
     * @param callable $createInstagramPhoto
     */
    public function __construct(
        SessionStorage $sessionStorage,
        FilesStorage $fileStorage,
        TemplateStorage $templateStorage,
        InstagramApi $api,
        callable $createInstagramPhoto
    )
    {
        $this->sessionStorage = $sessionStorage;
        $this->fileStorage = $fileStorage;
        $this->templateStorage = $templateStorage;
        $this->api = $api;
        $this->createInstagramPhoto = $createInstagramPhoto;
    }

    /**
     * Upload photo.
     * @throws \Exception\BadTemplateFile
     * @throws \Exception\LoginFailed
     * @throws \Exception\SessionWriteError
     * @throws \Exception\UploadError
     */
    public function upload()
    {
        $template = $this->templateStorage->get();
        $session = $this->sessionStorage->get($template);

        if ($session === null || $session->isUploaded()) {
            $files = iterator_to_array($this->fileStorage->getAll());
            $session = $this->sessionStorage->create($template, $files);
        }

        $upload = $session->getNotUploadedFile();

        if ($upload !== null) {
            try {
                $path = $upload->getFile()->getPath();
                $template = $session->getTemplate();
                $photo = call_user_func($this->createInstagramPhoto, $path);

                if ($photo) {
                    $this->api->login($template);

                    $this->api->upload($photo, $template);
                }
            }
            finally {
                $upload->setUploaded();
                $this->sessionStorage->save($session);
            }
        }
    }

    /**
     * Return period of start upload script.
     * @return int
     * @throws \Exception\BadTemplateFile
     */
    public function getPeriod()
    {
        $template = $this->templateStorage->get();

        return $template->getPeriod();
    }
}
