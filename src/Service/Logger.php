<?php

namespace Service;

use Psr\Log\LoggerInterface;

/**
 * Class Logger
 * @package Service
 */
class Logger
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Logger constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Log message.
     * @param string $message
     */
    public function log(string $message)
    {
        $this->logger->critical($message);
    }
}