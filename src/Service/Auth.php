<?php

namespace Service;

use Config;
use Exception;
use Serializer\Json;
use Entity\AuthPayload;
use Mapper\AuthPayloadMapper;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

const USER_ID = 1;

/**
 * Class Auth
 * @package Service
 */
class Auth
{
    /**
     * @var string
     */
    public static $KEY = 'auth';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var AuthPayloadMapper
     */
    protected $mapper;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * Auth constructor.
     * @param Config $config
     * @param AuthPayloadMapper $mapper
     * @param SessionInterface $session
     * @param Json $serializer
     */
    public function __construct(
        Config $config,
        AuthPayloadMapper $mapper,
        SessionInterface $session,
        Json $serializer
    )
    {
        $this->config = $config;
        $this->mapper = $mapper;
        $this->session = $session;
        $this->serializer = $serializer;
    }

    /**
     * Login user by username and password.
     * @param string $login
     * @param string $password
     * @return AuthPayload|null
     */
    public function login(string $login, string $password)
    {
        $configLogin = $this->config->getAdminLogin();
        $configPassword = $this->config->getAdminPassword();

        if ($configLogin === $login && $configPassword === $password) {
            $result = new AuthPayload(USER_ID, $login);
            $data = $this->mapper->encode($result);
            $json = $this->serializer->encode($data);

            $this->session->set(self::$KEY, $json);

            return $result;
        }

        return null;
    }

    /**
     * Return is user auth.
     * @return bool
     */
    public function isAuth()
    {
        return $this->session->has(self::$KEY);
    }

    /**
     * Return current user auth.
     * @return AuthPayload|null
     */
    public function get()
    {
        try {
            $data = $this->session->get(self::$KEY);
        } catch (Exception $e) {
            return null;
        }

        try {
            $json = $this->serializer->decode($data);

            return $this->mapper->decode($json);
        } catch (Exception $e) {
            return null;
        }
    }

    public function logout()
    {
        $this->session->remove(self::$KEY);
    }
}
