<?php

namespace Entity;

use Config;

const DEFAULT_PERIOD = 60;

/**
 * Class Template
 * @package Entity
 */
class Template
{
    /**
     * @var string
     */
    public static $PERIOD = 'period';

    /**
     * @var string
     */
    public static $INSTAGRAM_LOGIN = 'instagram_login';

    /**
     * @var string
     */
    public static $INSTAGRAM_PASSWORD = 'instagram_password';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var string
     */
    protected $capture;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var array
     */
    protected $meta;

    /**
     * Template constructor.
     * @param Config $config
     * @param string $capture
     * @param string $location
     * @param array $meta
     */
    public function __construct(Config $config, string $capture, string $location, array $meta = [])
    {
        $this->config = $config;
        $this->capture = $capture;
        $this->location = $location;
        $this->meta = $meta;
    }

    /**
     * Return caption of instagram template
     * @return string
     */
    public function getCaption()
    {
        return $this->capture;
    }

    /**
     * Return location.
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Return meta data.
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Return period of start upload script.
     * @return int
     */
    public function getPeriod()
    {
        return isset($this->meta[self::$PERIOD])
            ? intval($this->meta[self::$PERIOD])
            : DEFAULT_PERIOD;
    }

    /**
     * Return instagram account username.
     * @return string
     */
    public function getInstagramUsername()
    {
        return isset($this->meta[self::$INSTAGRAM_LOGIN])
            ? $this->meta[self::$INSTAGRAM_LOGIN]
            : $this->config->getInstagramUsername();
    }

    /**
     * Return instagram account password.
     * @return string
     */
    public function getInstagramPassword()
    {
        return isset($this->meta[self::$INSTAGRAM_PASSWORD])
            ? $this->meta[self::$INSTAGRAM_PASSWORD]
            : $this->config->getInstagramPassword();
    }
}
