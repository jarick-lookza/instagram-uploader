<?php

namespace Entity;

/**
 * Class Session
 * @package Entity
 */
class Session
{
    /**
     * @var Template
     */
    protected $template;

    /**
     * @var Upload[]
     */
    protected $uploads;

    /**
     * Session constructor.
     * @param Template $template
     * @param Upload[] $uploads
     */
    public function __construct(array $uploads)
    {
        $this->uploads = $uploads;
    }

    /**
     * Return array of upload files
     * @return Upload[]
     */
    public function getUploads()
    {
        return $this->uploads;
    }

    /**
     * Set instagram template.
     * @param Template $template
     * @return $this
     */
    public function setTemplate(Template $template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get instagram template
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Return all files of session is uploaded
     * @return bool
     */
    public function isUploaded()
    {
        foreach ($this->uploads as $upload) {
            if (!$upload->isUploaded()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return first not uploaded file
     * @return Upload|null
     */
    public function getNotUploadedFile()
    {
        foreach ($this->uploads as $upload) {
            if (!$upload->isUploaded()) {
                return $upload;
            }
        }

        return null;
    }
}
