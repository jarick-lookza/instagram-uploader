<?php

namespace Entity;

/**
 * Class AuthPayload
 * @package Entity
 */
class AuthPayload
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $username;

    /**
     * AuthPayload constructor.
     * @param int $id
     * @param string $username
     */
    public function __construct(int $id, string $username)
    {
        $this->id = $id;
        $this->username = $username;
    }

    /**
     * Return user id.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Return username.
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
}
