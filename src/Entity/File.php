<?php

namespace Entity;

/**
 * Class File
 * @package Entity
 */
class File
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    /**
     * File constructor.
     * @param string $name
     * @param string $path
     */
    public function __construct(string $name, string $path)
    {
        $this->name = $name;
        $this->path = $path;
    }

    /**
     * Return name of file
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return path of file
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
