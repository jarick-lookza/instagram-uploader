<?php

namespace Entity;

/**
 * Class InstagramAuth
 * @package Entity
 */
class InstagramAuth
{
    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * InstagramAuth constructor.
     * @param string $login
     * @param string $password
     */
    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * Return user login.
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Return user password.
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}
