<?php

namespace Entity;

/**
 * Class Upload
 * @package Entity
 */
class Upload
{
    /**
     * @var File
     */
    protected $file;

    /**
     * @var bool
     */
    protected $upload;

    /**
     * Upload constructor.
     * @param File $file
     * @param bool $upload
     */
    public function __construct(File $file, bool $upload)
    {
        $this->file = $file;
        $this->upload = $upload;
    }

    /**
     * Return file is uploaded
     * @return bool
     */
    public function isUploaded()
    {
        return $this->upload;
    }

    /**
     * Return upload file
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file is uploaded
     */
    public function setUploaded()
    {
        $this->upload = true;
    }
}
