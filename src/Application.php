<?php

use Service\InstagramUploader;
use Service\Logger;
use Exception\Exception;

class Application
{
    /**
     * @var InstagramUploader
     */
    protected $uploader;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Application constructor.
     * @param InstagramUploader $uploader
     * @param Logger $logger
     */
    public function __construct(InstagramUploader $uploader, Logger $logger)
    {
        $this->uploader = $uploader;
        $this->logger = $logger;
    }

    /**
     * Run upload
     */
    public function run()
    {
        try {
            $this->uploader->upload();
        } catch (Exception $e) {
            $this->logger->log($e->getMessage());
        }
    }

    /**
     * Return period of start upload script.
     * @return int
     * @throws \Exception\BadTemplateFile
     */
    public function getPeriod()
    {
        return $this->uploader->getPeriod();
    }
}
