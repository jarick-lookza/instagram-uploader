<?php

namespace Middleware;

use Controller\Base;
use Service\Auth;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class NeedLogin
 * @package Middleware
 */
class NeedLogin extends Base
{
    /**
     * @var Auth
     */
    protected $auth;

    /**
     * NeedLogin constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Invoke middleware.
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!$this->auth->isAuth()) {
            return $response->withStatus(StatusCode::HTTP_UNAUTHORIZED);
        }

        return $next($request, $response);
    }
}
