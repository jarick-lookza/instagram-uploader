<?php

namespace Middleware;

use Controller\Base;
use Controller\Login;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

class RedirectOnLogin extends Base
{
    /**
     * @var NeedLogin
     */
    protected $needLoginMiddleware;

    /**
     * RedirectOnLogin constructor.
     * @param NeedLogin $needLoginMiddleware
     */
    public function __construct(NeedLogin $needLoginMiddleware)
    {
        $this->needLoginMiddleware = $needLoginMiddleware;
    }

    /**
     * Invoke middleware.
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $result = $this->needLoginMiddleware->__invoke($request, $response, $next);

        if ($result->getStatusCode() === StatusCode::HTTP_UNAUTHORIZED) {
            return $this->redirect($result, Login::$PATH);
        }

        return $result;
    }
}
