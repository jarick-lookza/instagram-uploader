<?php

namespace Middleware;

use Controller\Base;
use Controller\Upload;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

class RedirectOnIndex extends Base
{
    /**
     * @var NotLogin
     */
    protected $notLoginMiddleware;

    /**
     * RedirectOnIndex constructor.
     * @param NotLogin $notLoginMiddleware
     */
    public function __construct(NotLogin $notLoginMiddleware)
    {
        $this->notLoginMiddleware = $notLoginMiddleware;
    }

    /**
     * Invoke middleware.
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $result = $this->notLoginMiddleware->__invoke($request, $response, $next);

        if ($result->getStatusCode() === StatusCode::HTTP_FORBIDDEN) {
            return $this->redirect($result, Upload::$PATH);
        }

        return $result;
    }
}
