<?php

const INSTAGRAM_LOGIN = 'INSTAGRAM_LOGIN';
const INSTAGRAM_PASSWORD = 'INSTAGRAM_PASSWORD';
const ADMIN_LOGIN = 'ADMIN_LOGIN';
const ADMIN_PASSWORD = 'ADMIN_PASSWORD';
const PERIOD = 'PERIOD';
const DEFAULT_PERIOD = 300;

/**
 * Class Config
 */
class Config
{
    /**
     * @var string
     */
    public static $PUBLIC_FOLDER = 'public';

    /**
     * @var string
     */
    public static $UPLOAD_FOLDER = 'upload';

    /**
     * @var string
     */
    public static $DATA_FOLDER = 'data';

    /**
     * @var string
     */
    public static $SESSION_FILE = 'session.json';

    /**
     * @var string
     */
    public static $TEMPLATE_FILE = 'template.json';

    /**
     * @var string
     */
    public static $SERVER_STATUS_FILE = 'server_meta_data.json';

    /**
     * @var string
     */
    public static $DS = DIRECTORY_SEPARATOR;

    /**
     * @var string
     */
    protected $root;

    /**
     * Config constructor.
     * @param string $root
     */
    public function __construct(string $root)
    {
        $this->root = $root;
    }

    /**
     * Return path to upload folder.
     * @return string
     */
    public function getUploadFolder()
    {
        return $this->root . self::$DS . self::$PUBLIC_FOLDER . self::$DS . self::$UPLOAD_FOLDER;
    }

    /**
     * Return path to template file.
     * @return string
     */
    public function getTemplateFile()
    {
        return $this->root . self::$DS . self::$DATA_FOLDER . self::$DS . self::$TEMPLATE_FILE;
    }

    /**
     * Return path to session file.
     * @return string
     */
    public function getSessionFile()
    {
        return $this->root . self::$DS . self::$DATA_FOLDER . self::$DS . self::$SESSION_FILE;
    }

    /**
     * Return path to file with server meta data.
     * @return string
     */
    public function getServerMetaFile()
    {
        return $this->root . self::$DS . self::$DATA_FOLDER . self::$DS . self::$SERVER_STATUS_FILE;
    }

    /**
     * Return instagram account password.
     * @return string
     */
    public function getInstagramPassword()
    {
        return getenv(INSTAGRAM_PASSWORD);
    }

    /**
     * Return instagram account username.
     * @return string
     */
    public function getInstagramUsername()
    {
        return getenv(INSTAGRAM_LOGIN);
    }

    /**
     * Return admin login.
     * @return string
     */
    public function getAdminLogin()
    {
        return getenv(ADMIN_LOGIN);
    }

    /**
     * Return admin password.
     * @return string
     */
    public function getAdminPassword()
    {
        return getenv(ADMIN_PASSWORD);
    }

    /**
     * Return period of start upload script.
     * @return number
     */
    public function getPeriod()
    {
        return (int) getenv(PERIOD) ?: DEFAULT_PERIOD;
    }
}
