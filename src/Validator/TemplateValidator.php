<?php

namespace Validator;

use Valitron\Validator;

class TemplateValidator extends Validator
{
    // @codeCoverageIgnoreStart
    public function __construct(array $data = array(), array $fields = array(), string $lang = null, string $langDir = null)
    {
        parent::__construct($data, $fields, $lang, $langDir);

        $this->rule('required', ['instagram_login', 'instagram_password', 'text', 'period']);
        $this->rule('optional', 'location');
        $this->rule('integer', 'period');
        $this->rule('min', 'period', 1);
    }
    // @codeCoverageIgnoreEnd
}
