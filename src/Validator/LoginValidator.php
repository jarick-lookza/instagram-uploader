<?php

namespace Validator;

use Controller\Login;
use Valitron\Validator;

class LoginValidator extends Validator
{
    // @codeCoverageIgnoreStart
    public function __construct(array $data = array(), array $fields = array(), string $lang = null, string $langDir = null)
    {
        parent::__construct($data, $fields, $lang, $langDir);

        $this->rule('required', [Login::$USERNAME, Login::$PASSWORD]);
    }
    // @codeCoverageIgnoreEnd
}
