<?php

namespace Socket;

const START_COMMAND = 'start';
const STOP_COMMAND = 'stop';
const RESTART_COMMAND = 'restart';
const STATUS_COMMAND = 'status';
const MAX_MSG_LENGTH = 1;
use Socket\Raw\Factory;

/**
 * Class Client
 * @package Socket
 */
class Client
{
    /**
     * @var Factory
     */
    protected $factory;

    /**
     * @var string
     */
    protected $address;

    /**
     * Client constructor.
     * @param Factory $factory
     * @param string $address
     */
    public function __construct(Factory $factory, string $address)
    {
        $this->factory = $factory;
        $this->address = $address;
    }

    /**
     * Return new connection to upload script.
     * @return Raw\Socket
     */
    private function getClient()
    {
        return $this->factory->createClient($this->address);
    }

    /**
     * Send start command to upload script.
     */
    public function start()
    {
        $socket = $this->getClient();
        $socket->write(START_COMMAND);
        $socket->close();
    }

    /**
     * Send stop command to upload script.
     */
    public function stop()
    {
        $socket = $this->getClient();
        $socket->write(STOP_COMMAND);
        $socket->close();
    }

    /**
     * Send restart command to upload script.
     */
    public function restart()
    {
        $socket = $this->getClient();
        $socket->write(RESTART_COMMAND);
        $socket->close();
    }

    /**
     * Return upload script is active.
     * @return bool
     */
    public function isActive()
    {
        $socket = $this->getClient();
        $socket->write(STATUS_COMMAND);
        $status = (int) $socket->read(MAX_MSG_LENGTH);
        $socket->close();

        return $status > 0;
    }
}
