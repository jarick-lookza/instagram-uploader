<?php

namespace Socket;

use RandomLib\Generator;
use React\EventLoop\LoopInterface;
use React\EventLoop\Timer\TimerInterface;
use React\Socket\ServerInterface;
use Repository\ServerMetaDataStorage;
use Socket\Listener\Connection;

const CONNECTION_EVENT = 'connection';
const LOWER_BOUND = 0;
const UPPER_BOUND = 5;

/**
 * Class Server
 * @package Socket
 */
class Server
{
    /**
     * @var LoopInterface
     */
    protected $loop;

    /**
     * @var ServerInterface
     */
    protected $socket;

    /**
     * @var Generator
     */
    protected $randomizer;

    /**
     * @var TimerInterface|null
     */
    protected $timer;

    /**
     * @var ServerMetaDataStorage
     */
    protected $serverMetaStorage;

    /**
     * Server constructor.
     * @param LoopInterface $loop
     * @param ServerInterface $socket
     * @param Generator $randomizer
     * @param ServerMetaDataStorage $serverMetaStorage
     */
    public function __construct(
        LoopInterface $loop,
        ServerInterface $socket,
        Generator $randomizer,
        ServerMetaDataStorage $serverMetaStorage
    )
    {
        $this->loop = $loop;
        $this->socket = $socket;
        $this->randomizer = $randomizer;
        $this->serverMetaStorage = $serverMetaStorage;
    }

    /**
     * Set timer.
     * @param TimerInterface|null $timer
     */
    public function setTimer($timer)
    {
        $this->timer = $timer;
    }

    /**
     * Start periodic timer which call $run function after $getPeriod + random time
     * @param callable $getPeriod
     * @param callable $run
     * @param bool $isCallUserFunction
     */
    public function startTimer(callable $getPeriod, callable $run, bool $isCallUserFunction = true)
    {
        if ($isCallUserFunction) {
            call_user_func($run);
        }
        $period = call_user_func($getPeriod);

        $random = $this->randomizer->generateInt(LOWER_BOUND, UPPER_BOUND);

        $nextTick = function () use ($getPeriod, $run) {
            // @codeCoverageIgnoreStart
            $this->startTimer($getPeriod, $run);
            // @codeCoverageIgnoreEnd
        };

        $this->timer = $this->loop->addTimer($period + $random, $nextTick);
        $this->serverMetaStorage->writeStatus(ServerMetaDataStorage::$STATUS_STARTED);
    }

    /**
     * Cancel timer.
     */
    public function cancelTimer()
    {
        if ($this->timer) {
            $this->timer->cancel();
        }
        $this->serverMetaStorage->writeStatus(ServerMetaDataStorage::$STATUS_STOPPED);
    }

    /**
     * Return is timer active.
     * @return bool
     */
    public function isActive()
    {
        if (!$this->timer) {
            return false;
        }

        return $this->timer->isActive();
    }

    /**
     * Listen socket.
     * @param Connection $connectionListener
     */
    public function listen(Connection $connectionListener)
    {
        $this->socket->on(CONNECTION_EVENT, $connectionListener);

        $dataListener = $connectionListener->getDataListener();
        $getPeriod = $dataListener->getPeriod;
        $run = $dataListener->run;

        $status = $this->serverMetaStorage->readStatus();

        if ($status === ServerMetaDataStorage::$STATUS_STARTED) {
            $this->startTimer($getPeriod, $run, false);
        }
    }
}
