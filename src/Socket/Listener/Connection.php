<?php

namespace Socket\Listener;

use React\Socket\ConnectionInterface;
use Socket\Server;

const DATA_EVENT = 'data';

/**
 * Class Connection
 * @package Socket\Listener
 */
class Connection
{
    /**
     * @var Data
     */
    protected $dataListener;

    /**
     * @var Server
     */
    protected $server;

    /**
     * @return Data
     */
    public function getDataListener()
    {
        return $this->dataListener;
    }

    /**
     * Connection constructor.
     * @param Server $server
     * @param Data $dataListener
     */
    public function __construct(Server $server, Data $dataListener)
    {
        $this->server = $server;
        $this->dataListener = $dataListener;
    }

    /**
     * Connection invoke.
     * @param ConnectionInterface $conn
     */
    public function __invoke(ConnectionInterface $conn)
    {
        $this->dataListener->setConnection($this->server, $conn);
        $conn->on(DATA_EVENT, $this->dataListener);
    }
}