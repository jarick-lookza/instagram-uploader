<?php

namespace Socket\Listener;

use React\Socket\ConnectionInterface;
use Socket\Server;

const START_COMMAND = 'start';
const STOP_COMMAND = 'stop';
const RESTART_COMMAND = 'restart';
const STATUS_COMMAND = 'status';
const STATUS_RUN = '1';
const STATUS_STOPPED = '0';

class Data
{
    /**
     * @var Server
     */
    protected $server;

    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * @var callable
     */
    public $getPeriod;

    /**
     * @var callable
     */
    public $run;

    public function __construct(callable $getPeriod, callable $run)
    {
        $this->getPeriod = $getPeriod;
        $this->run = $run;
    }

    public function setConnection(Server $server, ConnectionInterface $connection)
    {
        $this->server = $server;
        $this->connection = $connection;
    }

    public function __invoke(string $data)
    {
        switch ($data) {
            case START_COMMAND:
                if (!$this->server->isActive()) {
                    $this->server->startTimer($this->getPeriod, $this->run, false);
                }
                break;
            case STOP_COMMAND:
                if ($this->server->isActive()) {
                    $this->server->cancelTimer();
                }
                break;
            case RESTART_COMMAND:
                if ($this->server->isActive()) {
                    $this->server->cancelTimer();
                }
                $this->server->startTimer($this->getPeriod, $this->run, false);
                break;
            case STATUS_COMMAND:
                $active = $this->server->isActive();

                $this->connection->write($active ? STATUS_RUN : STATUS_STOPPED);
                break;
            default:
                throw new \RuntimeException("Unknown command `$data`");
        }
    }
}
