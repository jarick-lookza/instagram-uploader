<?php

namespace Repository;

use Exception\SessionWriteError;
use League\Flysystem\FilesystemInterface;
use Entity\Session;
use Entity\Template;
use Entity\File;
use Entity\Upload;
use Config;
use Exception;
use Mapper\SessionMapper;
use Serializer\Json;

/**
 * Class SessionStorage
 * @package Repository
 */
class SessionStorage
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var FilesystemInterface
     */
    protected $fileSystem;

    /**
     * @var SessionMapper
     */
    protected $mapper;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * SessionStorage constructor.
     * @param Config $config
     * @param FilesystemInterface $fileSystem
     * @param SessionMapper $mapper
     * @param Json $serializer
     */
    public function __construct(
        Config $config,
        FilesystemInterface $fileSystem,
        SessionMapper $mapper,
        Json $serializer
    ) {
        $this->config = $config;
        $this->fileSystem = $fileSystem;
        $this->mapper = $mapper;
        $this->serializer = $serializer;
    }

    /**
     * Return current session from disk.
     * @param Template $template
     * @return Session|null
     */
    public function get(Template $template)
    {
        $path = $this->config->getSessionFile();
        
        try {
            $data = $this->fileSystem->read($path);
        } catch (\Exception $e) {
            return null;
        }

        try {
            $json = $this->serializer->decode($data);

            if (is_array($json)) {
                $session = $this->mapper->decode($json);
                $session->setTemplate($template);

                return $session;
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Save session to disk.
     * @param Session $session
     * @throws SessionWriteError
     */
    public function save(Session $session)
    {
        $path = $this->config->getSessionFile();

        try {
            $json = $this->mapper->encode($session);
            $data = $this->serializer->encode($json);

            $this->fileSystem->put($path, $data);
        } catch (Exception $e) {
            throw new SessionWriteError();
        }
    }

    /**
     * @param File[] $files
     * @return \Generator
     */
    private function mapUploadFile(array $files)
    {
        foreach ($files as $file) {
            yield new Upload($file, false);
        }
    }

    /**
     * Create new session
     * @param Template $template
     * @param File[] $files
     * @return Session
     */
    public function create(Template $template, array $files)
    {
        $uploads = iterator_to_array($this->mapUploadFile($files));

        $result = new Session($uploads);
        return $result->setTemplate($template);
    }
}
