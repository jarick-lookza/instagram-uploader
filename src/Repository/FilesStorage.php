<?php

namespace Repository;

use Config;
use Exception;
use Exception\PhotoFileIsNotFound;
use League\Flysystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Entity\File;

class FilesStorage
{
    /**
     * @var array
     */
    static $FILE_EXT = ['*.jpg', '*.jpeg'];

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Finder
     */
    protected $finder;

    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * FilesStorage constructor.
     * @param Config $config
     * @param Finder $finder
     * @param Filesystem $fileSystem
     */
    public function __construct(Config $config, Finder $finder, Filesystem $fileSystem)
    {
        $this->config = $config;

        foreach (self::$FILE_EXT as $ext) {
            $finder->name($ext);
        }

        $sort = function (\SplFileInfo $a, \SplFileInfo $b) {
            // @codeCoverageIgnoreStart
            return ($b->getMTime() - $a->getMTime());
            // @codeCoverageIgnoreEnd
        };

        $finder->files()->in($config->getUploadFolder())->sort($sort);
        $this->finder = $finder;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Get all files in upload folder
     * @return \Generator
     */
    public function getAll()
    {
        foreach ($this->finder as $file) {
            $name = $file->getRelativePathname();
            $path = $file->getRealPath();
            yield new File($name, $path);
        }
    }

    /**
     * Return count of files in upload folder
     * @return int
     */
    public function getPhotosCount()
    {
        return $this->finder->count();
    }

    /**
     * Return file size iterator
     * @return \Generator
     */
    private function getPhotosSizeIterator()
    {
        foreach ($this->finder as $file) {
            yield $file->getSize();
        }
    }

    /**
     * Format numer of bytes to pretty string
     * @param $bytes
     * @param int $precision
     * @return string
     */
    private function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
     * Return size of upload folder.
     * @return string
     */
    public function getPhotosSize()
    {
        $size = array_sum(iterator_to_array($this->getPhotosSizeIterator()));

        return $this->formatBytes($size);
    }

    /**
     * Find file by name.
     * @param string $name
     * @return File|null
     */
    private function findFileByName(string $name)
    {
        /** @var File $file */
        foreach ($this->getAll() as $file) {
            if ($file->getName() === $name) {
                return $file;
            }
        }

        return null;
    }

    /**
     * Delete file
     * @param string $name
     * @throws PhotoFileIsNotFound
     */
    public function delete(string $name)
    {
        $file = $this->findFileByName($name);

        if ($file === null) {
            throw new PhotoFileIsNotFound($name);
        }

        try{
            $this->fileSystem->delete($file->getPath());
        } catch (Exception $e) {
            throw new PhotoFileIsNotFound($name);
        }
    }
}
