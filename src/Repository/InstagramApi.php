<?php

namespace Repository;

use Entity\InstagramAuth;
use Entity\Template;
use InstagramAPI\Instagram;
use Exception\LoginFailed;
use Exception\UploadError;
use Exception;
use InstagramAPI\Media\Photo\InstagramPhoto;
use InstagramAPI\Response\Model\Location;
use Serializer\Json;

/**
 * Class InstagramApi
 * @package Repository
 */
class InstagramApi
{
    /**
     * @var Instagram
     */
    protected $ig;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * InstagramApi constructor.
     * @param Instagram $ig
     * @param Json $serializer
     */
    public function __construct( Instagram $ig, Json $serializer)
    {
        $this->ig = $ig;
        $this->serializer = $serializer;
    }

    /**
     * Return user login and password for instagram.
     * @param Template $template
     * @return InstagramAuth
     */
    public function geAuthByTemplate(Template $template)
    {
        $username = $template->getInstagramUsername();
        $password = $template->getInstagramPassword();

        return new InstagramAuth($username, $password);
    }

    /**
     * Login to instagram.
     * @param Template $template
     * @throws LoginFailed
     */
    public function login(Template $template)
    {
        $auth = $this->geAuthByTemplate($template);

        try {
            $this->ig->login($auth->getLogin(), $auth->getPassword());
        } catch (Exception $e) {
            throw new LoginFailed();
        }
    }

    /**
     * Return list of find by query location.
     * @param string $query
     * @return \Generator
     */
    public function search(string $query)
    {
        $locations = $this->ig->location->search(54.7, 20.51, $query);

        foreach ($locations->getVenues() as $location) {
            yield [
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'address' => $location->getAddress(),
                'external_id' => $location->getExternalId(),
                'external_id_source' => $location->getExternalIdSource(),
                'name' => $location->getName(),
                'minimum_age' => 0,
            ];
        }

    }

    /**
     * Upload photo to instagram.
     * @param InstagramPhoto $photo
     * @param Template $template
     * @return bool
     * @throws UploadError
     */
    public function upload(InstagramPhoto $photo, Template $template)
    {
        $photoMeta = [
            'caption' => $template->getCaption(),
        ];

        if ($template->getLocation() !== '') {
            // @codeCoverageIgnoreStart
            $location = $this->serializer->decode($template->getLocation());

            $photoMeta['location'] = new Location($location);
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->ig->timeline->uploadPhoto($photo->getFile(), $photoMeta);
        } catch (Exception $e) {
            throw new UploadError();
        }

        return true;
    }
}
