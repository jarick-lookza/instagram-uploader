<?php

namespace Repository;

use Entity\Template;
use League\Flysystem\FilesystemInterface;
use Mapper\TemplateMapper;
use Config;
use Exception\BadTemplateFile;
use Serializer\Json;

/**
 * Class TemplateStorage
 * @package Repository
 */
class TemplateStorage
{
    public static $JSON_TYPE = 'json';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var FilesystemInterface
     */
    protected $fileSystem;

    /**
     * @var TemplateMapper
     */
    protected $mapper;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * TemplateStorage constructor.
     * @param Config $config
     * @param FilesystemInterface $fileSystem
     * @param TemplateMapper $mapper
     * @param Json $serializer
     */
    public function __construct(
        Config $config,
        FilesystemInterface $fileSystem,
        TemplateMapper $mapper,
        Json $serializer
    ) {
        $this->config = $config;
        $this->fileSystem = $fileSystem;
        $this->mapper = $mapper;
        $this->serializer = $serializer;
    }

    /**
     * Get current template.
     * @return Template
     * @throws BadTemplateFile
     */
    public function get()
    {
        $path = $this->config->getTemplateFile();

        try {
            $data = $this->fileSystem->read($path);
        } catch (\Exception $e) {
            return new Template($this->config, '', '');
        }

        try {
            $json = $this->serializer->decode($data);

            return $this->mapper->decode($json);
        } catch (\Exception $e) {
            throw new BadTemplateFile();
        }
    }

    /**
     * Save template on disk.
     * @param Template|\Prophecy\Argument\Token\TypeToken $template
     */
    public function save(Template $template)
    {
        $json = $this->mapper->encode($template);
        $data = $this->serializer->encode($json);
        $path = $this->config->getTemplateFile();

        $this->fileSystem->put($path, $data);
    }
}