<?php

namespace Repository;

use Config;
use League\Flysystem\Filesystem;
use Serializer\Json;
use Exception;

/**
 * Class ServerMetaDataStorage
 * @package Repository
 */
class ServerMetaDataStorage
{
    /**
     * @var string
     */
    public static $STATUS_STARTED = 'started';

    /**
     * @var string
     */
    public static $STATUS_STOPPED = 'stopped';

    /**
     * @var string
     */
    public static $KEY_STATUS = 'status';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * ServerMetaDataStorage constructor.
     * @param Config $config
     * @param Filesystem $fileSystem
     * @param Json $serializer
     */
    public function __construct(
        Config $config,
        Filesystem $fileSystem,
        Json $serializer
    )
    {
        $this->config = $config;
        $this->fileSystem = $fileSystem;
        $this->serializer = $serializer;
    }

    /**
     * Read current status from file.
     * @return string
     */
    public function readStatus()
    {
        try {
            $path = $this->config->getServerMetaFile();
            $json = $this->fileSystem->read($path);
            $data = $this->serializer->decode($json);
            $status = isset($data[self::$KEY_STATUS]) ? $data[self::$KEY_STATUS] : self::$STATUS_STARTED;
        } catch (Exception $e) {
            $status = self::$STATUS_STARTED;
        }

        return $status;
    }

    /**
     * Write status to disk.
     * @param string $status
     */
    public function writeStatus(string $status)
    {
        $path = $this->config->getServerMetaFile();
        $json = [self::$KEY_STATUS => $status];
        $data = $this->serializer->encode($json);
        $this->fileSystem->put($path, $data);
    }
}
