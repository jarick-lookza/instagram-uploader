<?php

namespace Mapper;

use Entity\AuthPayload;
use Prophecy\Argument;

/**
 * Class AuthPayloadMapper
 * @package Mapper
 */
class AuthPayloadMapper
{
    /**
     * Serialize auth payload to array
     * @param AuthPayload|Argument\Token\TypeToken $payload
     * @return array
     */
    public function encode(AuthPayload $payload)
    {
        return [
            'id' => $payload->getId(),
            'username' => $payload->getUsername(),
        ];
    }

    /**
     * Deserialize auth payload from array
     * @param array $json
     * @return AuthPayload
     */
    public function decode(array $json)
    {
        return new AuthPayload($json['id'], $json['username']);
    }
}
