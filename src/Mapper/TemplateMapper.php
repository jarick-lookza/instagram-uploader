<?php

namespace Mapper;

use Entity\Template;
use Prophecy\Argument\Token\TypeToken;
use Config;

/**
 * Class TemplateMapper
 * @package Mapper
 */
class TemplateMapper
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * TemplateMapper constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Serialize auth payload to array
     * @param Template|TypeToken $template
     * @return array
     */
    public function encode(Template $template)
    {
        return [
            'capture' => $template->getCaption(),
            'location' => $template->getLocation(),
            'meta' => $template->getMeta()
        ];
    }

    /**
     * Deserialize auth payload from array
     * @param array $json
     * @return Template
     */
    public function decode(array $json)
    {
        $capture = $json['capture'];
        $location = $json['location'];
        $meta = isset($json['meta']) ? $json['meta'] : [];

        return new Template($this->config, $capture, $location, $meta);
    }
}
