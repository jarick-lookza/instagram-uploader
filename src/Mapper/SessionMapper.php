<?php

namespace Mapper;

use Entity\File;
use Entity\Session;
use Entity\Upload;

/**
 * Class SessionMapper
 * @package Mapper
 */
class SessionMapper
{
    /**
     * Serialize session to array
     * @param Session $session
     * @return array
     */
    public function encode(Session $session)
    {
        $encode = ['uploads' => []];

        foreach ($session->getUploads() as $upload) {
            $encode['uploads'][] = [
                'uploaded' => $upload->isUploaded(),
                'file' => [
                    'name' => $upload->getFile()->getName(),
                    'path' => $upload->getFile()->getPath(),
                ],
            ];
        }

        return $encode;
    }

    /**
     * Convert array data to uploads.
     * @param array $json
     * @return \Generator
     */
    private function mapUploads(array $json)
    {
        foreach ($json['uploads'] as $uploadJson) {
            $uploaded = $uploadJson['uploaded'];
            $file = new File($uploadJson['file']['name'], $uploadJson['file']['path']);

            yield new Upload($file, $uploaded);
        }
    }

    /**
     * Deserialize session from array
     * @param array $json
     * @return Session
     */
    public function decode(array $json)
    {
        $uploads = iterator_to_array($this->mapUploads($json));

        return new Session($uploads);
    }
}
