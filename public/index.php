<?php
use Slim\Http\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;

const DEBUG = 'DEBUG';
const SOCKET = '/tmp/sid_server.sock';

if (PHP_SAPI == 'cli-server') {
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';
$session = new Session();
$session->start();

$dotEnv = new \Dotenv\Dotenv(dirname(__DIR__));
$dotEnv->load();

$config = new Config(dirname(__DIR__));

$finder = new \Symfony\Component\Finder\Finder();
$adapter = new \League\Flysystem\Adapter\Local('/');
\InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
$ig = new \InstagramAPI\Instagram();
$fileSystem = new \League\Flysystem\Filesystem($adapter);
$serializer = new \Serializer\Json();
$socketFactory = new \Socket\Raw\Factory();

$authPayloadMapper = new \Mapper\AuthPayloadMapper();
$templateMapper = new \Mapper\TemplateMapper($config);

$auth = new \Service\Auth($config, $authPayloadMapper, $session, $serializer);
$filesStorage = new \Repository\FilesStorage($config, $finder, $fileSystem);
$templateStorage = new \Repository\TemplateStorage($config, $fileSystem, $templateMapper, $serializer);
$client = new \Socket\Client($socketFactory, 'unix://' . SOCKET);
$api = new \Repository\InstagramApi($ig, $serializer);

$settings = [
    'settings' => [
        'displayErrorDetails' => getenv(DEBUG) ?: false,
    ],
];

$templatesFolderPath = __DIR__ . '/../templates/';
$renderer = new \Slim\Views\PhpRenderer($templatesFolderPath);

$app = new \Slim\App($settings);

$moveUploadedFile = function ($directory, UploadedFile $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    /** @noinspection PhpUnhandledExceptionInspection*/
    $basename = bin2hex(random_bytes(8));
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
};

$notLoginMiddleware = new \Middleware\NotLogin($auth);
$needLoginMiddleware = new \Middleware\NeedLogin($auth);
$redirectOnLoginMiddleware = new \Middleware\RedirectOnLogin($needLoginMiddleware);
$redirectOnIndexMiddleware = new \Middleware\RedirectOnIndex($notLoginMiddleware);

$templateValidator = new \Validator\TemplateValidator();
$loginValidator = new \Validator\LoginValidator();


$loginController = new \Controller\Login($config, $renderer, $auth, $loginValidator);
$uploadController = new \Controller\Upload($config, $renderer, $moveUploadedFile);
$mediaController = new \Controller\Media($renderer, $filesStorage);
$settingsController = new \Controller\Settings(
    $config,
    $renderer,
    $templateStorage,
    $client,
    $serializer,
    $api,
    $templateValidator
);

$app->get(\Controller\Login::$PATH, [$loginController, 'index'])
    ->add($redirectOnIndexMiddleware);
$app->post(\Controller\Login::$PATH, [$loginController, 'save'])
    ->add($notLoginMiddleware);
$app->get(\Controller\Login::$PATH_LOGOUT, [$loginController, 'logout'])
    ->add($redirectOnLoginMiddleware);
$app->get(\Controller\Upload::$PATH, [$uploadController, 'index'])
    ->add($redirectOnLoginMiddleware);
$app->post(\Controller\Upload::$PATH, [$uploadController, 'save'])
    ->add($needLoginMiddleware);
$app->get(\Controller\Media::$PATH_GALLERY, [$mediaController, 'getGallery'])
    ->add($needLoginMiddleware);
$app->get(\Controller\Media::$PATH, [$mediaController, 'index'])
    ->add($redirectOnLoginMiddleware);
$app->post(\Controller\Media::$PATH_DELETE, [$mediaController, 'delete'])
    ->add($needLoginMiddleware);
$app->get(\Controller\Media::$PATH_STATS, [$mediaController, 'getStats'])
    ->add($needLoginMiddleware);
$app->get(\Controller\Settings::$PATH, [$settingsController, 'get'])
    ->add($redirectOnLoginMiddleware);
$app->post(\Controller\Settings::$PATH, [$settingsController, 'save'])
    ->add($needLoginMiddleware);
$app->get(\Controller\Settings::$PATH_LOCATION, [$settingsController, 'search'])
    ->add($needLoginMiddleware);
$app->post(\Controller\Settings::$PATH_START_SCRIPT, [$settingsController, 'startScript'])
    ->add($needLoginMiddleware);
$app->post(\Controller\Settings::$PATH_STOP_SCRIPT, [$settingsController, 'stopScript'])
    ->add($needLoginMiddleware);

/** @noinspection PhpUnhandledExceptionInspection*/
$app->run();