<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__ . '/vendor/autoload.php';
const DEBUG = 'DEBUG';
const LOGGER_NAME = '';
const LOG_FILE = '/application.log';
const SOCKET = '/tmp/sid_server.sock';

$dotEnv = new \Dotenv\Dotenv(__DIR__);
$dotEnv->load();

$config = new Config(__DIR__);

$finder = new \Symfony\Component\Finder\Finder();
$adapter = new \League\Flysystem\Adapter\Local('/');
$fileSystem = new \League\Flysystem\Filesystem($adapter);
$ig = new \InstagramAPI\Instagram();
$sessionMapper = new \Mapper\SessionMapper();
$templateMapper = new \Mapper\TemplateMapper($config);
$serializer = new \Serializer\Json();
$factory = new RandomLib\Factory;
$randomizer = $factory->getLowStrengthGenerator();

$fileStorage = new \Repository\FilesStorage($config, $finder, $fileSystem);
$templateStorage = new \Repository\TemplateStorage($config, $fileSystem, $templateMapper, $serializer);
$sessionStorage = new \Repository\SessionStorage($config, $fileSystem, $sessionMapper, $serializer);
$serverMetaDataStorage = new \Repository\ServerMetaDataStorage($config, $fileSystem, $serializer);
$api = new \Repository\InstagramApi($ig, $serializer);

try {
    $logger = new \Monolog\Logger(LOGGER_NAME);
    $logHandler = new \Monolog\Handler\StreamHandler(__DIR__ . LOG_FILE);
    $logger->pushHandler($logHandler);
} catch (Exception $e) {
    echo 'Can not create log file';
    exit(0);
}

$loggerService = new \Service\Logger($logger);

$createInstagramPhoto = function (string $path) {
    try {
        return new \InstagramAPI\Media\Photo\InstagramPhoto($path);
    } catch (\Exception $e) {
        return null;
    }
};
$uploaderService = new \Service\InstagramUploader(
    $sessionStorage,
    $fileStorage,
    $templateStorage,
    $api,
    $createInstagramPhoto
);

$app = new Application($uploaderService, $loggerService);

if (getenv(DEBUG)) {
    $app->run();
} else {
    if (file_exists(SOCKET)) {
        unlink(SOCKET);
    }

    $loop = React\EventLoop\Factory::create();
    $socket = new React\Socket\Server('unix://' . SOCKET, $loop);
    $server = new \Socket\Server($loop, $socket, $randomizer, $serverMetaDataStorage);

    $dataListener = new \Socket\Listener\Data([$app, 'getPeriod'], [$app, 'run']);
    $connectionListener = new \Socket\Listener\Connection($server, $dataListener);

    $server->listen($connectionListener);

    $loop->run();
}
