[![build-status](https://img.shields.io/bitbucket/pipelines/jarick-lookza/instagram-uploader.svg)](https://bitbucket.org/jarick-lookza/instagram-uploader/addon/pipelines/home#!/) 
[![Coverage Status](https://coveralls.io/repos/github/jarick/instagram-uploader/badge.svg?branch=master)](https://coveralls.io/github/jarick/instagram-uploader?branch=master)	
# Instagram uploader

Periodic script upload photo to instagram

# Use cases

1. Rename `.ev.example` to `.env` file

3. Set your instagram username, password and period timer in `.env` file

4. Run `supervisor`. Config:

```
   [program:worker]
   command=php index.php
   directory=/path/to/project/instagram-uploader
   autostart=true
   autorestart=true
   
   [program:web]
   command=php -S 0.0.0.0:80 -t public
   directory=/path/to/project/instagram-uploader
   autostart=true
   autorestart=true
```
  