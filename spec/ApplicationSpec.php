<?php

namespace spec;

use Exception\Exception;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Service\InstagramUploader;
use Service\Logger;

class ApplicationSpec extends ObjectBehavior
{
    function let(
        InstagramUploader $uploader,
        Logger $logger
    )
    {
        $this->beConstructedWith($uploader, $logger);
    }

    function it_should_run(InstagramUploader $uploader)
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $uploader->upload()->shouldBeCalled();

        $this->run();
    }

    function it_should_log_error(
        InstagramUploader $uploader,
        Logger $logger
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $uploader->upload()->willThrow(Exception::class);
        $logger->log(Argument::type('string'))->shouldBeCalled();

        $this->run();
    }

    function it_should_return_period(
        InstagramUploader $uploader
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $uploader->getPeriod()->willReturn(60);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->getPeriod()->shouldReturn(60);
    }
}
