<?php

namespace spec\Repository;

use InstagramAPI\Instagram;
use InstagramAPI\Media\Photo\InstagramPhoto;
use Entity\Template;
use InstagramAPI\Request\Location;
use InstagramAPI\Request\Timeline;
use PhpSpec\ObjectBehavior;
use Exception\UploadError;
use Exception;
use Serializer\Json;
use Exception\LoginFailed;

const PATH = '~/test.jpg';
const CAPTION = 'text';
const LOCATION = '';
const PHOTO_META = [
    'caption' => CAPTION,
];
const USERNAME = 'test';
const PASSWORD = 'password';
const FILE_UPLOAD = 'file';

class InstagramApiSpec extends ObjectBehavior
{
    function let(Instagram $ig, Json $serializer, Template $template)
    {
        $template->getInstagramUsername()->willReturn(USERNAME);
        $template->getInstagramPassword()->willReturn(PASSWORD);
        $template->getCaption()->willReturn(CAPTION);
        $template->getLocation()->willReturn(LOCATION);

        $this->beConstructedWith($ig, $serializer);
    }

    function it_should_upload_photo_to_instagram(
        Instagram $ig,
        Timeline $timeline,
        Template $template,
        InstagramPhoto $photo
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $photo->getFile()->willReturn(FILE_UPLOAD);
        $timeline->uploadPhoto(FILE_UPLOAD, PHOTO_META)->shouldBeCalled();
        $ig->timeline = $timeline;

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->upload($photo, $template)->shouldReturn(true);
    }

    function it_should_throw_upload_exception(
        Instagram $ig,
        Timeline $timeline,
        Template $template,
        InstagramPhoto $photo
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $photo->getFile()->willReturn(FILE_UPLOAD);
        $timeline
            ->uploadPhoto(FILE_UPLOAD, PHOTO_META)
            ->willThrow(Exception::class);
        $ig->timeline = $timeline;

        $this->shouldThrow(UploadError::class)->during('upload', [$photo, $template]);
    }

    function it_should_return_result_of_search_location(
        Instagram $ig,
        Location $location
    )
    {
        $locationResponse = new class {
            public function getVenues() {
                return [new class {
                    function getLat() { return 1; }
                    function getLng() { return 2; }
                    function getAddress() { return 'address'; }
                    function getExternalId() { return 3; }
                    function getExternalIdSource() { return 'fb'; }
                    function getName() { return 'name'; }
                }];
            }
        };
        $location->search(54.7, 20.51, 'test')->willReturn($locationResponse);
        $ig->location = $location;

        $result = new \ArrayIterator([
            0 => [
                'lat' => 1,
                'lng' => 2,
                'address' => 'address',
                'external_id' => 3,
                'external_id_source' => 'fb',
                'name' => 'name',
                'minimum_age' => 0,
            ]
        ]);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->search('test')->shouldYieldLike($result);
    }

    function it_should_user_login(
        Template $template,
        Instagram $ig
    )
    {
        $ig->login(USERNAME, PASSWORD)->shouldBeCalled();

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->login($template);
    }

    function it_should_throw_login_exception(
        Template $template,
        Instagram $ig
    )
    {
        $ig->login(USERNAME, PASSWORD)->willThrow(Exception::class);

        $this->shouldThrow(LoginFailed::class)->during('login', [$template]);
    }
}
