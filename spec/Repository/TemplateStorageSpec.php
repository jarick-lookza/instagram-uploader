<?php

namespace spec\Repository;

use Entity\Template;
use Mapper\TemplateMapper;
use PhpSpec\ObjectBehavior;
use Config;
use Exception\BadTemplateFile;
use League\Flysystem\FilesystemInterface;
use Exception;
use Serializer\Json;
use const spec\Mapper\FILE_PATH;

const CAPTURE = 'test capture';
const FILE = '~/test.json';
const JSON_TEMPLATE_DATA = 'data';

class TemplateStorageSpec extends ObjectBehavior
{
    function let(
        Config $config,
        FilesystemInterface $fileSystem,
        TemplateMapper $mapper,
        Json $serializer
    )
    {
        $config->getTemplateFile()->willReturn(FILE);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(FILE)->willReturn(JSON_SESSION_DATA);
        $serializer->decode(JSON_SESSION_DATA)->willReturn([JSON_SESSION_DATA]);


        $this->beConstructedWith($config, $fileSystem, $mapper, $serializer);
    }

    function it_should_return_current_template(
        Template $template,
        TemplateMapper $mapper
    )
    {
        $mapper
            ->decode([JSON_SESSION_DATA])
            ->willReturn($template);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->get()->shouldBeLike($template);
    }

    function it_should_throw_template_file_not_exists_exception(
        FilesystemInterface $fileSystem,
        Config $config
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(FILE)->willThrow(Exception::class);

        $template = new Template($config->getWrappedObject(),'', '');
        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->get()->shouldBeLike($template);
    }

    function it_should_throw_decode_file_exception(
        FilesystemInterface $fileSystem,
        TemplateMapper $mapper
    )
    {
        $mapper
            ->decode([JSON_SESSION_DATA])
            ->willThrow(Exception::class);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(FILE)->willReturn(JSON_SESSION_DATA);

        $this->shouldThrow(BadTemplateFile::class)->during('get');
    }

    function it_should_save_template_on_disk(
        Config $config,
        FilesystemInterface $fileSystem,
        TemplateMapper $mapper,
        Json $serializer,
        Template $template
    )
    {
        $mapper->encode($template)->willReturn([JSON_TEMPLATE_DATA]);
        $serializer->encode([JSON_TEMPLATE_DATA])->willReturn(JSON_TEMPLATE_DATA);
        $config->getTemplateFile()->willReturn(FILE_PATH);

        $fileSystem->put(FILE_PATH, JSON_TEMPLATE_DATA)->shouldBeCalled();

        $this->save($template);
    }
}
