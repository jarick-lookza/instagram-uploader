<?php

namespace spec\Repository;

use Entity\File;
use Entity\Template;
use Entity\Upload;
use Mapper\SessionMapper;
use PhpSpec\ObjectBehavior;
use League\Flysystem\FilesystemInterface;
use Entity\Session;
use Config;
use Exception;
use Exception\SessionWriteError;
use Serializer\Json;

const SESSION_FILE = '~/session.json';
const JSON_SESSION_DATA = 'data';

class SessionStorageSpec extends ObjectBehavior
{
    function let(
        Config $config,
        FilesystemInterface $fileSystem,
        SessionMapper $mapper,
        Json $serializer
    )
    {
        $this->beConstructedWith($config, $fileSystem, $mapper, $serializer);
    }

    function it_should_return_null_when_file_read_failed(
        Config $config,
        FilesystemInterface $fileSystem,
        Template $template
    )
    {
        $config->getSessionFile()->willReturn(SESSION_FILE);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(SESSION_FILE)->willThrow(Exception::class);

        $this->get($template)->shouldReturn(null);
    }

    function it_should_return_null_when_session_serialize_failed(
        Config $config,
        FilesystemInterface $fileSystem,
        SessionMapper $mapper,
        Template $template,
        Json $serializer
    )
    {
        $config->getSessionFile()->willReturn(SESSION_FILE);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(SESSION_FILE)->willReturn(JSON_SESSION_DATA);
        $serializer->decode(JSON_SESSION_DATA)->willReturn([JSON_SESSION_DATA]);
        $mapper->decode([JSON_SESSION_DATA])->willThrow(Exception::class);

        $this->get($template)->shouldReturn(null);
    }

    function it_should_return_null_when_session_serialize_failed_2(
        Config $config,
        FilesystemInterface $fileSystem,
        Template $template,
        Json $serializer
    )
    {
        $config->getSessionFile()->willReturn(SESSION_FILE);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(SESSION_FILE)->willReturn(JSON_SESSION_DATA);
        $serializer->decode(JSON_SESSION_DATA)->willReturn(null);

        $this->get($template)->shouldReturn(null);
    }

    function it_should_return_current_session(
        Config $config,
        FilesystemInterface $fileSystem,
        SessionMapper $mapper,
        Session $session,
        Template $template,
        Json $serializer
    )
    {
        $config->getSessionFile()->willReturn(SESSION_FILE);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(SESSION_FILE)->willReturn(JSON_SESSION_DATA);
        $serializer->decode(JSON_SESSION_DATA)->willReturn([JSON_SESSION_DATA]);
        $mapper->decode([JSON_SESSION_DATA])->willReturn($session);
        $session->setTemplate($template)->willReturn($session);

        $this->get($template)->shouldReturn($session);
    }

    function it_should_save_session(
        Config $config,
        FilesystemInterface $fileSystem,
        SessionMapper $mapper,
        Session $session,
        Json $serializer
    )
    {
        $config->getSessionFile()->willReturn(SESSION_FILE);
        $mapper->encode($session)->willReturn(JSON_SESSION_DATA);
        $serializer->encode(JSON_SESSION_DATA)->willReturn(JSON_SESSION_DATA);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->put(SESSION_FILE, JSON_SESSION_DATA)->shouldBeCalled();

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->save($session);
    }

    function it_should_throw_session_write_exception(
        Config $config,
        FilesystemInterface $fileSystem,
        SessionMapper $mapper,
        Session $session,
        Json $serializer
    )
    {
        $config->getSessionFile()->willReturn(SESSION_FILE);
        $mapper->encode($session)->willReturn(JSON_SESSION_DATA);
        $serializer->encode(JSON_SESSION_DATA)->willReturn(JSON_SESSION_DATA);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->put(SESSION_FILE, JSON_SESSION_DATA)->willThrow(Exception::class);

        $this->shouldThrow(SessionWriteError::class)->during('save', [$session]);
    }

    function it_should_create_new_session(
        Template $template,
        File $file
    )
    {
        $uploads = [new Upload($file->getWrappedObject(), false)];
        $result = new Session($uploads);
        $result->setTemplate($template->getWrappedObject());

        $this->create($template->getWrappedObject(), [$file->getWrappedObject()])->shouldBeLike($result);
    }
}
