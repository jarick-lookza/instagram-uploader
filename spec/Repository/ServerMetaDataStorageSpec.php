<?php

namespace spec\Repository;

use Exception;
use Config;
use League\Flysystem\Filesystem;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Repository\ServerMetaDataStorage;use Serializer\Json;

const META_FILE = '~/file.json';
const JSON_META_DATA = 'json';

class ServerMetaDataStorageSpec extends ObjectBehavior
{
    function let(
        Config $config,
        Filesystem $fileSystem,
        Json $serializer
    )
    {
        $config->getServerMetaFile()->willReturn(META_FILE);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(META_FILE)->willReturn(JSON_META_DATA);
        $serializer->decode(JSON_META_DATA)->willReturn([JSON_META_DATA]);
        $serializer->encode(Argument::type('array'))->willReturn(JSON_META_DATA);
        $fileSystem->put(META_FILE, JSON_META_DATA)->willReturn(true);


        $this->beConstructedWith($config, $fileSystem, $serializer);
    }

    function it_should_return_current_status()
    {
        $this->readStatus()->shouldReturn(ServerMetaDataStorage::$STATUS_STARTED);
    }

    function it_should_save_status()
    {
        $this->writeStatus(ServerMetaDataStorage::$STATUS_STARTED);
    }

    function it_should_return_started_status_when_file_not_exists(Filesystem $fileSystem)
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->read(META_FILE)->willThrow(Exception::class);

        $this->readStatus()->shouldReturn(ServerMetaDataStorage::$STATUS_STARTED);
    }
}
