<?php

namespace spec\Repository;

use Exception;
use Exception\PhotoFileIsNotFound;
use League\Flysystem\Filesystem;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Config;
use Entity\File as FileEntity;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

const UPLOAD_FOLDER = '~/upload_test';
const FILE_NAME = 'photo.jpg';

class FilesStorageSpec extends ObjectBehavior
{
    function let(
        Config $config,
        Finder $finder,
        SplFileInfo $fileInfo,
        Filesystem $fileSystem
    )
    {
        $config->getUploadFolder()->willReturn(UPLOAD_FOLDER);

        $pathWithName = UPLOAD_FOLDER . Config::$DS . FILE_NAME;

        $fileInfo->getRelativePathname()->willReturn(FILE_NAME);
        $fileInfo->getRealPath()->willReturn($pathWithName);
        $fileInfo->getSize()->willReturn(1);

        /** @noinspection PhpParamsInspection */
        $finder->sort(Argument::type('closure'))->willReturn($finder);
        $finder->files()->willReturn($finder);
        /** @noinspection PhpParamsInspection */
        $finder->name(Argument::type('string'))->willReturn($finder);
        $finder->in(UPLOAD_FOLDER)->willReturn($finder);
        $finderResult = new \ArrayIterator([$fileInfo->getWrappedObject()]);
        $finder->getIterator()->willReturn($finderResult);
        $finder->count()->willReturn(1);

        $this->beConstructedWith($config, $finder, $fileSystem);
    }

    function it_should_get_all_files()
    {
        $pathWithName = UPLOAD_FOLDER . Config::$DS . FILE_NAME;
        $file = new FileEntity(FILE_NAME, $pathWithName);

        $result = new \ArrayIterator([$file]);
        $this->getAll()->shouldYieldLike($result);
    }

    function it_should_return_count_files()
    {
        $this->getPhotosCount()->shouldReturn(1);
    }

    function it_should_return_size_of_upload_folder()
    {
        $this->getPhotosSize()->shouldReturn('1 B');
    }

    function it_should_delete_file()
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->delete(FILE_NAME);
    }

    function it_should_throw_photo_not_found_exception_when_file_not_exists(Finder $finder)
    {
        $finderResult = new \ArrayIterator([]);
        $finder->getIterator()->willReturn($finderResult);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->shouldThrow(PhotoFileIsNotFound::class)->during('delete', [FILE_NAME]);
    }

    function it_should_throw_photo_not_found_exception_when_file_can_not_be_delete(Filesystem $fileSystem)
    {
        $pathWithName = UPLOAD_FOLDER . Config::$DS . FILE_NAME;
        /** @noinspection PhpUnhandledExceptionInspection*/
        $fileSystem->delete($pathWithName)->willThrow(Exception::class);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->shouldThrow(PhotoFileIsNotFound::class)->during('delete', [FILE_NAME]);
    }
}
