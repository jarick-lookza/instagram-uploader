<?php

namespace spec\Controller;

use Controller\Settings;
use Entity\InstagramAuth;
use Entity\Template;
use PhpSpec\ObjectBehavior;
use Config;
use Exception;
use Prophecy\Argument;
use Repository\InstagramApi;
use Repository\TemplateStorage;
use Serializer\Json;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Slim\Views\PhpRenderer;
use Socket\Client;
use Validator\TemplateValidator;

class SettingsSpec extends ObjectBehavior
{
    function let(
        Config $config,
        PhpRenderer $renderer,
        TemplateStorage $templateStorage,
        Client $client,
        Json $serializer,
        InstagramApi $api,
        Template $template,
        InstagramAuth $auth,
        TemplateValidator $validator
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $templateStorage->get()->willReturn($template);
        $template->getPeriod()->willReturn(60);
        $json = '{"name": "test"}';
        $template->getLocation()->willReturn($json);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $api->geAuthByTemplate($template)->willReturn($auth);
        $auth->getLogin()->willReturn('test_login');
        $auth->getPassword()->willReturn('test_password');
        $serializer->decode($json)->willReturn(['name' => 'test']);
        $template->getCaption()->willReturn('caption');

        $validator->withData([])->willReturn($validator);
        $validator->validate()->willReturn(true);

        $this->beConstructedWith(
            $config,
            $renderer,
            $templateStorage,
            $client,
            $serializer,
            $api,
            $validator
        );
    }

    function it_should_render_settings_page_when_upload_script_is_active(
        Request $request,
        Response $response,
        PhpRenderer $renderer,
        Client $client
    )
    {
        $client->isActive()->willReturn(true);
        $json = '{"name": "test"}';
        $renderer->render($response, Settings::$TEMPLATE, [
            'instagram_login' => 'test_login',
            'instagram_password' => 'test_password',
            'location' => $json,
            'location_name' => 'test',
            'text' => 'caption',
            'period' => 60,
            'upload_script_is_active' => true,
            'upload_script_is_running' => true,
        ])->willReturn($response);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->get($request, $response)->shouldReturn($response);
    }

    function it_should_render_settings_page_when_upload_script_is_not_active(
        Request $request,
        Response $response,
        PhpRenderer $renderer,
        Client $client
    )
    {
        $client->isActive()->willThrow(Exception::class);
        $json = '{"name": "test"}';
        $renderer->render($response, Settings::$TEMPLATE, [
            'instagram_login' => 'test_login',
            'instagram_password' => 'test_password',
            'location' => $json,
            'location_name' => 'test',
            'text' => 'caption',
            'period' => 60,
            'upload_script_is_active' => false,
            'upload_script_is_running' => false,
        ])->willReturn($response);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->get($request, $response)->shouldReturn($response);
    }

    function it_should_save_template(
        Request $request,
        Response $response,
        TemplateStorage $templateStorage,
        Client $client
    )
    {
        $request->getParams()->willReturn([]);
        $request->getParam('instagram_login')->willReturn('test_login');
        $request->getParam('instagram_password')->willReturn('test_password');
        $request->getParam('location', '')->willReturn('');
        $request->getParam('text')->willReturn('caption');
        $request->getParam('period')->willReturn(60);

        $templateStorage->save(Argument::type(Template::class))->shouldBeCalled();

        $response->withStatus(StatusCode::HTTP_ACCEPTED)->willReturn($response);
        $client->isActive()->willReturn(true);
        $client->restart()->shouldBeCalled();

        $this->save($request, $response)->shouldReturn($response);
    }

    function it_should_save_template_when_server_change_status_throw_exception(
        Request $request,
        Response $response,
        TemplateStorage $templateStorage,
        Client $client
    )
    {
        $request->getParams()->willReturn([]);
        $request->getParam('instagram_login')->willReturn('test_login');
        $request->getParam('instagram_password')->willReturn('test_password');
        $request->getParam('location', '')->willReturn('');
        $request->getParam('text')->willReturn('caption');
        $request->getParam('period')->willReturn(60);

        $templateStorage->save(Argument::type(Template::class))->shouldBeCalled();

        $response->withStatus(StatusCode::HTTP_ACCEPTED)->willReturn($response);
        $client->isActive()->willThrow(Exception::class);

        $this->save($request, $response)->shouldReturn($response);
    }

    function it_should_send_400_status(
        Request $request,
        Response $response,
        TemplateValidator $validator
    ){
        $request->getParams()->willReturn([]);
        $validator->validate()->willReturn(false);
        $validator->errors()->willReturn([]);
        $response->withJson([])->willReturn($response);
        $response->withStatus(StatusCode::HTTP_BAD_REQUEST)->willReturn($response);

        $this->save($request, $response)->shouldReturn($response);
    }

    function it_should_start_upload_script(
        Request $request,
        Response $response,
        Client $client
    )
    {
        $client->start()->shouldBeCalled();

        $response->withStatus(StatusCode::HTTP_ACCEPTED)->willReturn($response);
        $this->startScript($request, $response)->shouldReturn($response);
    }

    function it_should_stop_upload_script(
        Request $request,
        Response $response,
        Client $client
    )
    {
        $client->stop()->shouldBeCalled();

        $response->withStatus(StatusCode::HTTP_ACCEPTED)->willReturn($response);
        $this->stopScript($request, $response)->shouldReturn($response);
    }

    function it_should_return_empty_list_when_query_is_empty(
        Request $request,
        Response $response
    )
    {
        $request->getQueryParam('search', '')->willReturn('');

        $response->withJson([])->willReturn($response);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->search($request, $response)->shouldReturn($response);
    }

    function it_should_return_location_list_by_query(
        Request $request,
        Response $response,
        TemplateStorage $templateStorage,
        Template $template,
        InstagramApi $api
    )
    {
        $request->getQueryParam('search', '')->willReturn('q');
        /** @noinspection PhpUnhandledExceptionInspection*/
        $templateStorage->get()->willReturn($template);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $api->login($template)->shouldBeCalled();
        $api->search('q')->willReturn(new \ArrayIterator(['name.json']));

        $response->withJson(['name.json'])->willReturn($response);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->search($request, $response)->shouldReturn($response);
    }
}
