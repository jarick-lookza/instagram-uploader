<?php

namespace spec\Controller;

use Controller\Base;
use Controller\Login;
use Controller\Upload;
use PhpSpec\ObjectBehavior;
use Service\Auth;
use Slim\Http\Request;
use Slim\Http\Response;
use Config;
use Slim\Http\StatusCode;
use Slim\Views\PhpRenderer;
use Validator\LoginValidator;

class LoginSpec extends ObjectBehavior
{
    function let(
        Config $config,
        PhpRenderer $renderer,
        Auth $auth,
        LoginValidator $validator
    )
    {
        $validator->withData([])->willReturn($validator);
        $validator->validate()->willReturn(true);

        $this->beConstructedWith($config, $renderer, $auth, $validator);
    }

    function it_should_render_login_page(
        PhpRenderer $renderer,
        Request $request,
        Response $response
    )
    {
        $request->getParams()->willReturn([]);
        $template = Login::$TEMPLATE;
        $renderer->render($response, $template)->willReturn($response);

        $this->index($request, $response)->shouldReturn($response);
    }

    function it_should_redirect_to_index_page(
        Auth $auth,
        Request $request,
        Response $response
    )
    {
        $request->getParams()->willReturn([]);
        $request->getParam(Login::$USERNAME)->willReturn(Login::$USERNAME);
        $request->getParam(Login::$PASSWORD)->willReturn(Login::$PASSWORD);
        $auth->login(Login::$USERNAME, Login::$PASSWORD)->willReturn(true);
        $response->withStatus(StatusCode::HTTP_ACCEPTED)->willReturn($response);

        $this->save($request, $response)->shouldReturn($response);
    }

    function it_should_render_login_page_when_auth_is_failed(
        Auth $auth,
        Request $request,
        Response $response
    )
    {
        $request->getParams()->willReturn([]);
        $request->getParam(Login::$USERNAME)->willReturn(Login::$USERNAME);
        $request->getParam(Login::$PASSWORD)->willReturn(Login::$PASSWORD);
        $auth->login(Login::$USERNAME, Login::$PASSWORD)->willReturn(false);
        $response->withStatus(StatusCode::HTTP_BAD_REQUEST)->willReturn($response);

        $this->save($request, $response)->shouldReturn($response);
    }

    function it_should_send_400_status(
        Request $request,
        Response $response,
        LoginValidator $validator
    ){
        $request->getParams()->willReturn([]);
        $validator->validate()->willReturn(false);
        $validator->errors()->willReturn([]);
        $response->withJson([])->willReturn($response);
        $response->withStatus(StatusCode::HTTP_BAD_REQUEST)->willReturn($response);

        $this->save($request, $response)->shouldReturn($response);
    }

    function it_should_logout(
        Auth $auth,
        Request $request,
        Response $response
    )
    {
        $auth->logout()->shouldBeCalled();
        $response->withHeader(Base::$LOCATION, Upload::$PATH)->willReturn($response);
        $response->withStatus(StatusCode::HTTP_FOUND)->willReturn($response);

        $this->logout($request, $response)->shouldReturn($response);
    }
}
