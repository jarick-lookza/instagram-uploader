<?php

namespace spec\Controller;

use Controller\Upload;
use PhpSpec\ObjectBehavior;
use Slim\Http\Request;
use Slim\Http\Response;
use Config;
use Slim\Http\StatusCode;
use Slim\Http\UploadedFile;
use Slim\Views\PhpRenderer;

const UPLOAD_FOLDER = '~/upload';

class UploadSpec extends ObjectBehavior
{
    function let(Config $config, PhpRenderer $renderer)
    {
        $moveUploadedFile = function() {};

        $this->beConstructedWith($config, $renderer, $moveUploadedFile);
    }

    function it_should_render_upload_page(
        Request $request,
        Response $response,
        PhpRenderer $renderer
    )
    {
        $renderer->render($response, Upload::$TEMPLATE)->willReturn($response);

        $this->index($request, $response)->shouldReturn($response);
    }

    function it_should_redirect_to_uploaded_page(
        Request $request,
        Response $response,
        Config $config,
        UploadedFile $file,
        $uploadFolderPath
    )
    {
        $config->getUploadFolder()->willReturn($uploadFolderPath);
        $request->getUploadedFiles()->willReturn([$file]);
        $file->getError()->willReturn(UPLOAD_ERR_OK);
        $response->withStatus(StatusCode::HTTP_CREATED)->willReturn($response);

        $this->save($request, $response)->shouldReturn($response);
    }
}
