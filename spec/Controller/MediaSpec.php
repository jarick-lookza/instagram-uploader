<?php

namespace spec\Controller;

use Controller\Media;
use Entity\File;
use Exception\PhotoFileIsNotFound;
use PhpSpec\ObjectBehavior;
use Repository\FilesStorage;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Slim\Views\PhpRenderer;

class MediaSpec extends ObjectBehavior
{
    function let(PhpRenderer $renderer, FilesStorage $filesStorage)
    {
        $this->beConstructedWith($renderer, $filesStorage);
    }

    function it_should_render_media_page(
        Request $request,
        Response $response,
        PhpRenderer $renderer,
        FilesStorage $filesStorage
    )
    {
        $filesStorage->getPhotosSize()->willReturn(1);
        $filesStorage->getPhotosCount()->willReturn(1);

        $params = [
            'size' => '1 B',
            'count' => 1,
        ];
        $renderer->render($response, Media::$TEMPLATE, $params)->willReturn($response);

        $this->index($request, $response)->shouldReturn($response);
    }

    function it_should_return_gallery_image_list(
        Request $request,
        Response $response,
        PhpRenderer $renderer,
        FilesStorage $filesStorage,
        File $file1,
        File $file2
    )
    {
        $file1->getName()->willReturn('file1.jpg');
        $file1->getPath()->willReturn('~/file1.jpg');

        $file2->getName()->willReturn('file2.jpg');
        $file2->getPath()->willReturn('~/file2.jpg');

        $filesStorage->getAll()->willReturn([$file1, $file2]);


        $request->getQueryParam('cursor', '')->willReturn('');
        $request->getQueryParam('limit', Media::$DEFAULT_PAGINATE)->willReturn(1);

        $response->withJson(['file1.jpg'])->willReturn($response);
        $renderer->render($response, Media::$PATH_GALLERY)->willReturn($response);

        $this->getGallery($request, $response)->shouldReturn($response);
    }

    function it_should_return_gallery_image_list_when_cursor_is_set(
        Request $request,
        Response $response,
        PhpRenderer $renderer,
        FilesStorage $filesStorage,
        File $file1,
        File $file2
    )
    {
        $file1->getName()->willReturn('file1.jpg');
        $file1->getPath()->willReturn('~/file1.jpg');

        $file2->getName()->willReturn('file2.jpg');
        $file2->getPath()->willReturn('~/file2.jpg');

        $filesStorage->getAll()->willReturn([$file1, $file2]);
        $renderer->render($response, Media::$PATH_GALLERY)->willReturn($response);

        $response->withJson(['file2.jpg'])->willReturn($response);

        $request->getQueryParam('cursor', '')->willReturn('file1.jpg');
        $request->getQueryParam('limit', Media::$DEFAULT_PAGINATE)->willReturn(1);

        $this->getGallery($request, $response)->shouldReturn($response);
    }

    function it_should_delete_file(
        Request $request,
        Response $response,
        FilesStorage $filesStorage
    )
    {
        $response->withStatus(StatusCode::HTTP_ACCEPTED)->willReturn($response);

        $request->getParam('name', '')->willReturn('file2.jpg');

        /** @noinspection PhpUnhandledExceptionInspection*/
        $filesStorage->delete('file2.jpg')->shouldBeCalled();

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->delete($request, $response)->shouldReturn($response);
    }

    function it_should_send_404_error_when_delete_file_not_found(
        Request $request,
        Response $response,
        FilesStorage $filesStorage
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $filesStorage->delete('file2.jpg')->willThrow(PhotoFileIsNotFound::class);

        $request->getParam('name', '')->willReturn('file2.jpg');

        $response->withStatus(StatusCode::HTTP_NOT_FOUND)->willReturn($response);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->delete($request, $response)->shouldReturn($response);
    }

    function it_should_send_400_error_when_delete_file_is_not_set(
        Request $request,
        Response $response
    )
    {
        $request->getParam('name', '')->willReturn('');

        $response->withStatus(StatusCode::HTTP_BAD_REQUEST)->willReturn($response);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->delete($request, $response)->shouldReturn($response);
    }

    function it_should_return_files_stats(
        Request $request,
        Response $response,
        FilesStorage $filesStorage
    )
    {
        $filesStorage->getPhotosSize()->willReturn(1);
        $filesStorage->getPhotosCount()->willReturn(1);

        $data = [
            'size' => '1 B',
            'count' => 1,
        ];
        $response->withJson($data)->willReturn($response);

        $this->getStats($request, $response)->shouldReturn($response);
    }
}
