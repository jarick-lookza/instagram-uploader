<?php

namespace spec\Middleware;

use Controller\Base;
use Controller\Login;
use Middleware\NeedLogin;
use PhpSpec\ObjectBehavior;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

class RedirectOnLoginSpec extends ObjectBehavior
{
    function let(NeedLogin $needLoginMiddleware)
    {
        $this->beConstructedWith($needLoginMiddleware);
    }

    function it_should_do_anything(
        NeedLogin $needLoginMiddleware,
        Request $request,
        Response $response
    )
    {
        $response->getStatusCode()->willReturn(StatusCode::HTTP_OK);
        $next = function (/** @noinspection PhpUnusedParameterInspection */ $request, $response) {
            return $response;
        };
        $needLoginMiddleware->__invoke($request, $response, $next)->willReturn($response);

        $this->__invoke($request, $response, $next)->shouldReturn($response);
    }

    function it_should_redirect_to_index_page(
        NeedLogin $needLoginMiddleware,
        Request $request,
        Response $response
    )
    {
        $response->getStatusCode()->willReturn(StatusCode::HTTP_UNAUTHORIZED);
        $response->withHeader(Base::$LOCATION, Login::$PATH)->willReturn($response);
        $response->withStatus(StatusCode::HTTP_FOUND)->willReturn($response);
        $next = function (/** @noinspection PhpUnusedParameterInspection */ $request, $response) {
            return $response;
        };
        $needLoginMiddleware->__invoke($request, $response, $next)->willReturn($response);

        $this->__invoke($request, $response, $next)->shouldReturn($response);
    }
}
