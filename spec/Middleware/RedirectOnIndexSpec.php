<?php

namespace spec\Middleware;

use Controller\Base;
use Controller\Upload;
use Middleware\NotLogin;
use PhpSpec\ObjectBehavior;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

class RedirectOnIndexSpec extends ObjectBehavior
{
    function let(NotLogin $notLoginMiddleware)
    {
        $this->beConstructedWith($notLoginMiddleware);
    }

    function it_should_do_anything(
        NotLogin $notLoginMiddleware,
        Request $request,
        Response $response
    )
    {
        $response->getStatusCode()->willReturn(StatusCode::HTTP_OK);
        $next = function (/** @noinspection PhpUnusedParameterInspection */ $request, $response) {
            return $response;
        };
        $notLoginMiddleware->__invoke($request, $response, $next)->willReturn($response);

        $this->__invoke($request, $response, $next)->shouldReturn($response);
    }

    function it_should_redirect_to_index_page(
        NotLogin $notLoginMiddleware,
        Request $request,
        Response $response
    )
    {
        $response->getStatusCode()->willReturn(StatusCode::HTTP_FORBIDDEN);
        $response->withHeader(Base::$LOCATION, Upload::$PATH)->willReturn($response);
        $response->withStatus(StatusCode::HTTP_FOUND)->willReturn($response);
        $next = function (/** @noinspection PhpUnusedParameterInspection */ $request, $response) {
            return $response;
        };
        $notLoginMiddleware->__invoke($request, $response, $next)->willReturn($response);

        $this->__invoke($request, $response, $next)->shouldReturn($response);
    }
}
