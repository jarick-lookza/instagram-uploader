<?php

namespace spec\Middleware;

use PhpSpec\ObjectBehavior;
use Service\Auth;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

class NotLoginSpec extends ObjectBehavior
{
    function let(Auth $auth)
    {
        $this->beConstructedWith($auth);
    }

    function it_should_be_invoke_route(Request $request, Response $response, Auth $auth)
    {
        $next = function (/** @noinspection PhpUnusedParameterInspection */ $request, $response) {
            return $response;
        };
        $auth->isAuth()->willReturn(false);

        $this->__invoke($request, $response, $next)->shouldReturn($response);
    }

    function it_should_be_redirect_to_index_page(Request $request, Response $response, Auth $auth)
    {
        $next = function (/** @noinspection PhpUnusedParameterInspection */ $request, $response) {
            return $response;
        };
        $auth->isAuth()->willReturn(true);
        $response->withStatus(StatusCode::HTTP_FORBIDDEN)->willReturn($response);

        $this->__invoke($request, $response, $next)->shouldReturn($response);
    }
}
