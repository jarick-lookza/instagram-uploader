<?php

namespace spec\Socket;

use PhpSpec\ObjectBehavior;
use const Socket\MAX_MSG_LENGTH;
use Socket\Raw\Factory;
use Socket\Raw\Socket;
use const Socket\RESTART_COMMAND;
use const Socket\START_COMMAND;
use const Socket\STATUS_COMMAND;
use const Socket\STOP_COMMAND;

const ADDRESS = 'test_address';

class ClientSpec extends ObjectBehavior
{
    function let(Factory $factory, Socket $socket)
    {
        $socket->close()->shouldBeCalled();
        $factory->createClient(ADDRESS)->willReturn($socket);

        $this->beConstructedWith($factory, ADDRESS);
    }

    function it_should_send_start_script_command(Socket $socket)
    {
        $socket->write(START_COMMAND)->shouldBeCalled();

        $this->start();
    }

    function it_should_send_stop_script_command(Socket $socket)
    {
        $socket->write(STOP_COMMAND)->shouldBeCalled();

        $this->stop();
    }

    function it_should_send_restart_script_command(Socket $socket)
    {
        $socket->write(RESTART_COMMAND)->shouldBeCalled();

        $this->restart();
    }

    function it_should_return_true_when_upload_script_is_active(Socket $socket)
    {
        $socket->write(STATUS_COMMAND)->shouldBeCalled();
        $socket->read(MAX_MSG_LENGTH)->willReturn('1');

        $this->isActive()->shouldReturn(true);
    }
}
