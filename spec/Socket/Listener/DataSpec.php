<?php

namespace spec\Socket\Listener;

use Prophecy\Argument;
use React\Socket\ConnectionInterface;
use PhpSpec\ObjectBehavior;
use const Socket\Listener\RESTART_COMMAND;
use const Socket\Listener\START_COMMAND;
use const Socket\Listener\STATUS_COMMAND;
use const Socket\Listener\STATUS_RUN;
use const Socket\Listener\STATUS_STOPPED;
use const Socket\Listener\STOP_COMMAND;
use Socket\Server;

class DataSpec extends ObjectBehavior
{
    function let(
        Server $server,
        ConnectionInterface $connection
    )
    {
        $getPeriod =  function () { return 1; };
        $run = function () {};

        $this->beConstructedWith($getPeriod, $run);

        $this->getWrappedObject()->setConnection(
            $server->getWrappedObject(),
            $connection->getWrappedObject()
        );
    }

    function it_should_invoke_when_data_is_start_command(
        Server $server
    )
    {
        $server->isActive()->willReturn(false);
        /** @noinspection PhpParamsInspection */
        $server->startTimer(
            Argument::type('callable'),
            Argument::type('callable'),
            false
        )->shouldBeCalled();

        $this->__invoke(START_COMMAND);
    }

    function it_should_invoke_when_data_is_stop_command(
        Server $server
    )
    {
        $server->isActive()->willReturn(true);
        $server->cancelTimer()->shouldBeCalled();

        $this->__invoke(STOP_COMMAND);
    }

    function it_should_invoke_when_data_is_restart_command(
        Server $server
    )
    {
        $server->isActive()->willReturn(true);
        $server->cancelTimer()->shouldBeCalled();
        /** @noinspection PhpParamsInspection */
        $server->startTimer(
            Argument::type('callable'),
            Argument::type('callable'),
            false
        )->shouldBeCalled();

        $this->__invoke(RESTART_COMMAND);
    }

    function it_should_throw_exception_when_command_is_invalid()
    {
        $this->shouldThrow(\RuntimeException::class)->during('__invoke', ['qwerty']);
    }

    function it_should_return_status_run_when_timer_is_active(
        Server $server,
        ConnectionInterface $connection
    )
    {
        $server->isActive()->willReturn(true);
        $connection->write(STATUS_RUN)->shouldBeCalled();

        $this->__invoke(STATUS_COMMAND);
    }

    function it_should_return_status_stop_when_timer_is_cancel(
        Server $server,
        ConnectionInterface $connection
    )
    {
        $server->isActive()->willReturn(false);
        $connection->write(STATUS_STOPPED)->shouldBeCalled();

        $this->__invoke(STATUS_COMMAND);
    }
}
