<?php

namespace spec\Socket\Listener;

use PhpSpec\ObjectBehavior;
use React\Socket\ConnectionInterface;
use Socket\Listener\Data;
use const Socket\Listener\DATA_EVENT;
use Socket\Server;

class ConnectionSpec extends ObjectBehavior
{
    function let(Server $server, Data $dataListener)
    {
        $this->beConstructedWith($server, $dataListener);
    }

    function it_should_invoke(
        ConnectionInterface $conn,
        Data $dataListener,
        Server $server
    )
    {
        $dataListener->setConnection($server, $conn)->shouldBeCalled();
        $conn->on(DATA_EVENT, $dataListener)->shouldBeCalled();

        $this->__invoke($conn);
    }

    function it_should_return_data_listener(Data $dataListener)
    {
        $this->getDataListener()->shouldReturn($dataListener);
    }
}
