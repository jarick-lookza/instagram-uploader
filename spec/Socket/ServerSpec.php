<?php

namespace spec\Socket;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use React\EventLoop\Timer\TimerInterface;
use Repository\ServerMetaDataStorage;
use const Socket\CONNECTION_EVENT;
use Socket\Listener\Connection;
use RandomLib\Generator;
use React\EventLoop\LoopInterface;
use React\Socket\ServerInterface;
use Socket\Listener\Data;
use const Socket\LOWER_BOUND;
use const Socket\UPPER_BOUND;

class ServerSpec extends ObjectBehavior
{
    function let(
        LoopInterface $loop,
        ServerInterface $socket,
        Generator $randomizer,
        TimerInterface $timer,
        ServerMetaDataStorage $serverMetaDataStorage
    )
    {
        $this->beConstructedWith(
            $loop,
            $socket,
            $randomizer,
            $serverMetaDataStorage
        );

        $this->getWrappedObject()->setTimer($timer->getWrappedObject());
    }

    function it_should_cancel_timer(
        TimerInterface $timer,
        ServerMetaDataStorage $serverMetaDataStorage
    )
    {
        $status = ServerMetaDataStorage::$STATUS_STOPPED;

        $timer->cancel()->shouldBeCalled();
        $serverMetaDataStorage->writeStatus($status)->shouldBeCalled();

        $this->cancelTimer();
    }

    function it_should_return_timer_is_active(
        TimerInterface $timer
    )
    {
        $timer->isActive()->willReturn(true);

        $this->isActive()->shouldReturn(true);
    }

    function it_should_return_false_when_timer_is_not_set()
    {
        $this->setTimer(null);

        $this->isActive()->shouldReturn(false);
    }

    function it_should_listen_socket(
        ServerInterface $socket,
        Connection $connectionListener,
        LoopInterface $loop,
        Generator $randomizer,
        TimerInterface $timer,
        Data $dataListener,
        ServerMetaDataStorage $serverMetaDataStorage
    )
    {
        $socket->on(CONNECTION_EVENT, $connectionListener)->shouldBeCalled();

        $dataListener->getPeriod = function () { return 10; };
        $dataListener->run = function () { };
        $randomizer->generateInt(LOWER_BOUND, UPPER_BOUND)->willReturn(1);
        /** @noinspection PhpParamsInspection */
        $loop->addTimer(11, Argument::any())->willReturn($timer);
        $connectionListener->getDataListener()->willReturn($dataListener);
        $status = ServerMetaDataStorage::$STATUS_STARTED;
        $serverMetaDataStorage->readStatus()->willReturn($status);
        $serverMetaDataStorage->writeStatus($status)->shouldBeCalled();

        $this->listen($connectionListener);
    }

    function it_should_start_timer_and_call_user_function(
        LoopInterface $loop,
        Generator $randomizer,
        TimerInterface $timer,
        ServerMetaDataStorage $serverMetaDataStorage
    )
    {
        $getPeriod = function () { return 10; };
        $run = function () { };
        $randomizer->generateInt(LOWER_BOUND, UPPER_BOUND)->willReturn(1);
        /** @noinspection PhpParamsInspection */
        $loop->addTimer(11, Argument::any())->willReturn($timer);
        $status = ServerMetaDataStorage::$STATUS_STARTED;
        $serverMetaDataStorage->readStatus()->willReturn($status);
        $serverMetaDataStorage->writeStatus($status)->shouldBeCalled();

        $this->startTimer($getPeriod, $run);
    }
}
