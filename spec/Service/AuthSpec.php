<?php

namespace spec\Service;

use Entity\AuthPayload;
use Mapper\AuthPayloadMapper;
use PhpSpec\ObjectBehavior;
use Config;
use Exception;
use Prophecy\Argument;
use Serializer\Json;
use Service\Auth;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

const ADMIN_LOGIN = 'admin';
const ADMIN_PASSWORD = 'password';
const BAD_ADMIN_PASSWORD = 'bad_password';
const TOKEN = 'token';

class AuthSpec extends ObjectBehavior
{
    function let(
        Config $config,
        AuthPayloadMapper $mapper,
        SessionInterface $session,
        Json $serializer
    )
    {
        $config->getAdminLogin()->willReturn(ADMIN_LOGIN);
        $config->getAdminPassword()->willReturn(ADMIN_PASSWORD);

        $this->beConstructedWith($config, $mapper, $session, $serializer);
    }

    function it_should_login_by_password(
        AuthPayloadMapper $mapper,
        SessionInterface $session,
        Json $serializer
    )
    {
        $serializer->encode(TOKEN)->willReturn(TOKEN);
        $data = Argument::type(AuthPayload::class);
        $mapper->encode($data)->willReturn(TOKEN);
        $session->set(Auth::$KEY, TOKEN)->shouldBeCalled();

        $this
            ->login(ADMIN_LOGIN, ADMIN_PASSWORD)
            ->shouldBeAnInstanceOf(AuthPayload::class);
    }

    function it_should_return_null_when_password_is_failed()
    {
        $this->login(ADMIN_LOGIN, BAD_ADMIN_PASSWORD)->shouldReturn(null);
    }

    function it_should_return_user_is_auth(SessionInterface $session)
    {
        $session->has(Auth::$KEY)->willReturn(true);

        $this->isAuth()->shouldReturn(true);
    }

    function it_should_return_null_when_user_is_not_logged(SessionInterface $session)
    {
        $session->get(Auth::$KEY)->willThrow(Exception::class);

        $this->get()->shouldReturn(null);
    }

    function it_should_return_auth_payload(
        SessionInterface $session,
        AuthPayloadMapper $mapper,
        AuthPayload $payload,
        Json $serializer
    )
    {
        $session->get(Auth::$KEY)->willReturn(TOKEN);
        $serializer->decode(TOKEN)->willReturn([TOKEN]);
        $mapper->decode([TOKEN])->willReturn($payload);

        $this->get()->shouldReturn($payload);
    }

    function it_should_return_null_when_auth_payload_serialize_failed(
        SessionInterface $session,
        AuthPayloadMapper $mapper,
        Json $serializer
    )
    {
        $session->get(Auth::$KEY)->willReturn(TOKEN);
        $serializer->decode(TOKEN)->willReturn([TOKEN]);
        $mapper->decode([TOKEN])->willThrow(Exception::class);

        $this->get()->shouldReturn(null);
    }

    function it_should_logout(SessionInterface $session)
    {
        $session->remove(Auth::$KEY)->shouldBeCalled();

        $this->logout();
    }
}
