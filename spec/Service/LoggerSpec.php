<?php

namespace spec\Service;

use PhpSpec\ObjectBehavior;
use Psr\Log\LoggerInterface;

const LOG_MESSAGE = 'msg';

class LoggerSpec extends ObjectBehavior
{
    function let(LoggerInterface $logger)
    {
        $this->beConstructedWith($logger);
    }

    function it_should_log_message(LoggerInterface $logger)
    {
        $logger->critical(LOG_MESSAGE)->shouldBeCalled();

        $this->log(LOG_MESSAGE);
    }
}
