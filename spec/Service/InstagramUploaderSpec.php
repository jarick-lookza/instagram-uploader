<?php

namespace spec\Service;

use Entity\File;
use Entity\Session;
use Entity\Template;
use Entity\Upload;
use InstagramAPI\Media\Photo\InstagramPhoto;
use PhpSpec\ObjectBehavior;
use Repository\FilesStorage;
use Repository\SessionStorage;
use Repository\TemplateStorage;
use Repository\InstagramApi;

const PATH = '~/path';

class InstagramUploaderSpec extends ObjectBehavior
{
    function let(
        SessionStorage $sessionStorage,
        FilesStorage $fileStorage,
        TemplateStorage $templateStorage,
        InstagramApi $api,
        InstagramPhoto $instagramPhoto
    )
    {
        $createInstagramPhoto = function () use ($instagramPhoto) {
            return $instagramPhoto->getWrappedObject();
        };

        $this->beConstructedWith(
            $sessionStorage,
            $fileStorage,
            $templateStorage,
            $api,
            $createInstagramPhoto
        );
    }

    function it_should_upload_photo_when_session_is_exists(
        SessionStorage $sessionStorage,
        TemplateStorage $templateStorage,
        InstagramApi $api,
        Session $session,
        Upload $upload,
        File $file,
        Template $template,
        InstagramPhoto $instagramPhoto
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $templateStorage->get()->willReturn($template);

        $sessionStorage->get($template)->willReturn($session);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $sessionStorage->save($session)->shouldBeCalled();

        $session->isUploaded()->willReturn(false);
        $session->getNotUploadedFile()->willReturn($upload);
        $session->getTemplate()->willReturn($template);

        $upload->getFile()->willReturn($file);
        $upload->setUploaded()->shouldBeCalled();

        $file->getPath()->willReturn(PATH);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $api->login($template)->shouldBeCalled();
        /** @noinspection PhpUnhandledExceptionInspection*/
        $api->upload($instagramPhoto, $template)->shouldBeCalled();

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->upload();
    }

    function it_should_upload_photo_when_session_is_not_exists(
        FilesStorage $fileStorage,
        TemplateStorage $templateStorage,
        SessionStorage $sessionStorage,
        InstagramApi $api,
        Session $session,
        Upload $upload,
        File $file,
        Template $template,
        InstagramPhoto $instagramPhoto
    )
    {
        $fileStorage->getAll()->willReturn(new \ArrayIterator([$file]));

        /** @noinspection PhpUnhandledExceptionInspection*/
        $templateStorage->get()->willReturn($template);

        $sessionStorage->create($template, [$file])->willReturn($session);
        $sessionStorage->get($template)->willReturn($session);
        /** @noinspection PhpUnhandledExceptionInspection*/
        $sessionStorage->save($session)->shouldBeCalled();

        $session->isUploaded()->willReturn(true);
        $session->getNotUploadedFile()->willReturn($upload);
        $session->getTemplate()->willReturn($template);

        $upload->getFile()->willReturn($file);
        $upload->setUploaded()->shouldBeCalled();

        $file->getPath()->willReturn(PATH);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $api->login($template)->shouldBeCalled();
        /** @noinspection PhpUnhandledExceptionInspection*/
        $api->upload($instagramPhoto, $template)->shouldBeCalled();

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->upload();
    }

    function it_should_return_period(
        TemplateStorage $templateStorage,
        Template $template
    )
    {
        /** @noinspection PhpUnhandledExceptionInspection*/
        $templateStorage->get()->willReturn($template);
        $template->getPeriod()->willReturn(60);

        /** @noinspection PhpUnhandledExceptionInspection*/
        $this->getPeriod()->shouldReturn(60);
    }
}
