<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Dotenv\Dotenv;

const INSTAGRAM_USERNAME = 'test';
const INSTAGRAM_PASSWORD = 'password';
const ADMIN_LOGIN = 'admin';
const ADMIN_PASSWORD = 'password';
const PERIOD = 60;

class ConfigSpec extends ObjectBehavior
{
    function let()
    {
        $dotenv = new Dotenv(__DIR__);
        $dotenv->load();

        $path = dirname(__DIR__);
        $this->beConstructedWith($path);
    }

    function it_should_return_upload_folder()
    {
        $path = dirname(__DIR__);
        $this->getUploadFolder()->shouldReturn($path . '/public/upload');
    }

    function it_should_return_template_file()
    {
        $path = dirname(__DIR__);
        $this->getTemplateFile()->shouldReturn($path . '/data/template.json');
    }

    function it_should_return_session_file()
    {
        $path = dirname(__DIR__);
        $this->getSessionFile()->shouldReturn($path . '/data/session.json');
    }

    function it_should_return_instagram_username()
    {
        $this->getInstagramUsername()->shouldReturn(INSTAGRAM_USERNAME);
    }

    function it_should_return_instagram_password()
    {
        $this->getInstagramPassword()->shouldReturn(INSTAGRAM_PASSWORD);
    }

    function it_should_return_admin_login()
    {
        $this->getAdminLogin()->shouldReturn(ADMIN_LOGIN);
    }

    function it_should_return_admin_password()
    {
        $this->getAdminPassword()->shouldReturn(ADMIN_PASSWORD);
    }

    function it_should_return_period()
    {
        $this->getPeriod()->shouldReturn(PERIOD);
    }

    function it_should_return_path_to_server_meta_data()
    {
        $path = dirname(__DIR__);
        $this->getServerMetaFile()->shouldReturn($path . '/data/server_meta_data.json');
    }
}
