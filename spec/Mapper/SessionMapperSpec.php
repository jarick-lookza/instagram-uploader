<?php

namespace spec\Mapper;

use Entity\File;
use Entity\Session;
use Entity\Upload;
use Mapper\SessionMapper;
use PhpSpec\ObjectBehavior;

const FILE_NAME = 't.jpg';
const FILE_PATH = '~/t.jpg';
const JSON_SESSION_DATA = [
    'uploads' => [
        0 => [
            'uploaded' => false,
            'file' => [
                'name' => FILE_NAME,
                'path' => FILE_PATH,
            ]
        ]
    ]
];

class SessionMapperSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SessionMapper::class);
    }

    function it_should_encode_session()
    {
        $file = new File(FILE_NAME, FILE_PATH);
        $uploads = [new Upload($file, false)];
        $session = new Session($uploads);

        $this->encode($session)->shouldReturn(JSON_SESSION_DATA);
    }

    function it_should_decode_session()
    {
        $this->decode(JSON_SESSION_DATA)->shouldBeAnInstanceOf(Session::class);
    }
}
