<?php

namespace spec\Mapper;

use Entity\AuthPayload;
use Mapper\AuthPayloadMapper;
use PhpSpec\ObjectBehavior;

const USER_ID = 1;
const USER_NAME = 'admin';
const JSON_AUTH_PAYLOAD_DATA = [
    'id' => USER_ID,
    'username' => USER_NAME,
];

class AuthPayloadMapperSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(AuthPayloadMapper::class);
    }

    function it_should_encode_session()
    {
        $authPayload = new AuthPayload(USER_ID, USER_NAME);

        $this->encode($authPayload)->shouldReturn(JSON_AUTH_PAYLOAD_DATA);
    }

    function it_should_decode_session()
    {
        $this->decode(JSON_AUTH_PAYLOAD_DATA)->shouldBeAnInstanceOf(AuthPayload::class);
    }
}
