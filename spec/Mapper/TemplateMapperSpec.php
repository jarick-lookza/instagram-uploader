<?php

namespace spec\Mapper;

use Entity\Template;
use Config;
use PhpSpec\ObjectBehavior;

const CAPTURE = 'test';
const LOCATION = '';
const JSON_TEMPLATE_DATA = [
    'capture' => CAPTURE,
    'location' => LOCATION,
    'meta' => [],
];

class TemplateMapperSpec extends ObjectBehavior
{
    function let(Config $config)
    {
        $this->beConstructedWith($config);
    }

    function it_should_encode_template(Template $template)
    {
        $template->getCaption()->willReturn(CAPTURE);
        $template->getLocation()->willReturn(LOCATION);
        $template->getMeta()->willReturn([]);

        $this->encode($template)->shouldReturn(JSON_TEMPLATE_DATA);
    }

    function it_should_decode_template()
    {
        $this->decode(JSON_TEMPLATE_DATA)->shouldBeAnInstanceOf(Template::class);
    }
}
