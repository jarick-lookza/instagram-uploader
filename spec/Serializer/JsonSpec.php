<?php

namespace spec\Serializer;

use Serializer\Json;
use PhpSpec\ObjectBehavior;

const DATA = 'test';
const DATA_JSON = '"test"';

class JsonSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Json::class);
    }

    function it_should_encode_json()
    {
        $this->encode(DATA)->shouldReturn(DATA_JSON);
    }

    function it_should_throw_invalid_exception()
    {
        $this
            ->shouldThrow(\InvalidArgumentException::class)
            ->during('decode', ['{' . DATA_JSON]);
    }

    function it_should_decode_json()
    {
        $this->decode(DATA_JSON)->shouldReturn(DATA);
    }
}
