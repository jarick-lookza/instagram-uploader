<?php

namespace spec\Entity;

use PhpSpec\ObjectBehavior;

const USER_IG_LOGIN = 'test';
const USER_IG_PASSWORD = 'pass';

class InstagramAuthSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(USER_IG_LOGIN, USER_IG_PASSWORD);
    }

    function it_should_get_login()
    {
        $this->getLogin()->shouldReturn(USER_IG_LOGIN);
    }

    function it_should_get_password()
    {
        $this->getPassword()->shouldReturn(USER_IG_PASSWORD);
    }
}
