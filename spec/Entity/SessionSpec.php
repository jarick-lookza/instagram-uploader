<?php

namespace spec\Entity;

use PhpSpec\ObjectBehavior;
use Entity\Template;
use Entity\Upload;

class SessionSpec extends ObjectBehavior
{
    function let(Upload $upload)
    {
        $this->beConstructedWith([$upload]);
    }

    function it_should_return_uploads(Upload $upload)
    {
        $this->getUploads()->shouldReturn([$upload]);
    }

    function it_should_return_template(Template $template)
    {
        $this->setTemplate($template);

        $this->getTemplate()->shouldReturn($template);
    }

    function it_should_return_not_is_uploaded(Upload $upload)
    {
        $upload->isUploaded()->willReturn(false);

        $this->isUploaded()->shouldReturn(false);
    }

    function it_should_return_is_uploaded(Upload $upload)
    {
        $upload->isUploaded()->willReturn(true);

        $this->isUploaded()->shouldReturn(true);
    }

    function it_should_return_uploaded_file(Upload $upload)
    {
        $upload->isUploaded()->willReturn(false);

        $this->getNotUploadedFile()->shouldReturn($upload);
    }

    function it_should_return_null(Upload $upload)
    {
        $upload->isUploaded()->willReturn(true);

        $this->getNotUploadedFile()->shouldReturn(null);
    }
}
