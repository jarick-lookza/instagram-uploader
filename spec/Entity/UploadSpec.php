<?php

namespace spec\Entity;

use PhpSpec\ObjectBehavior;
use Entity\File;

class UploadSpec extends ObjectBehavior
{
    function let(File $file)
    {
        $this->beConstructedWith($file, false);
    }

    function it_should_get_uploaded()
    {
        $this->isUploaded()->shouldReturn(false);
    }

    function it_should_get_file($file)
    {
        $this->getFile()->shouldReturn($file);
    }

    function it_should_set_uploaded()
    {
        $this->setUploaded();
        $this->isUploaded()->shouldReturn(true);
    }
}
