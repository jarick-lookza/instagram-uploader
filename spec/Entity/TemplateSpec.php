<?php

namespace spec\Entity;

use Entity\Template;
use Config;
use PhpSpec\ObjectBehavior;

const CAPTURE = 'capture';
const LOCATION = 'location';
const INSTAGRAM_LOGIN = 'test';
const INSTAGRAM_PASSWORD = 'pass';
const PERIOD = 600;

class TemplateSpec extends ObjectBehavior
{
    function let(Config $config)
    {
        $this->beConstructedWith($config, CAPTURE, LOCATION);
    }

    function it_should_get_caption()
    {
        $this->getCaption()->shouldReturn(CAPTURE);
    }

    function it_should_get_location()
    {
        $this->getLocation()->shouldReturn(LOCATION);
    }

    function it_should_get_meta()
    {
        $this->getMeta()->shouldReturn([]);
    }

    function it_should_return_period_when_period_is_set(Config $config)
    {
        $meta = [Template::$PERIOD => PERIOD];
        $this->beConstructedWith($config, CAPTURE, LOCATION, $meta);

        $this->getPeriod()->shouldReturn(PERIOD);
    }

    function it_should_return_default_period_when_period_is_not_set()
    {
        $this->getPeriod()->shouldReturn(\Entity\DEFAULT_PERIOD);
    }

    function it_should_return_instagram_login_when_instagram_login_is_set(
        Config $config
    )
    {
        $meta = [Template::$INSTAGRAM_LOGIN => INSTAGRAM_LOGIN];
        $this->beConstructedWith($config, CAPTURE, LOCATION, $meta);

        $this->getInstagramUsername()->shouldReturn(INSTAGRAM_LOGIN);
    }

    function it_should_return_default_instagram_login_when_instagram_login_is_not_set(
        Config $config
    )
    {
        $config->getInstagramUsername()->willReturn(INSTAGRAM_LOGIN);
        $this->getInstagramUsername()->shouldReturn(INSTAGRAM_LOGIN);
    }

    function it_should_return_instagram_password_when_instagram_password_is_set(
        Config $config
    )
    {
        $meta = [Template::$INSTAGRAM_PASSWORD => INSTAGRAM_PASSWORD];
        $this->beConstructedWith($config, CAPTURE, LOCATION, $meta);

        $this->getInstagramPassword()->shouldReturn(INSTAGRAM_PASSWORD);
    }

    function it_should_return_default_instagram_password_when_instagram_password_is_not_set(
        Config $config
    )
    {
        $config->getInstagramPassword()->willReturn(INSTAGRAM_PASSWORD);
        $this->getInstagramPassword()->shouldReturn(INSTAGRAM_PASSWORD);
    }
}
