<?php

namespace spec\Entity;

use PhpSpec\ObjectBehavior;

const USER_ID = 1;
const USER_NAME = 'admin';

class AuthPayloadSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(USER_ID, USER_NAME);
    }

    function it_should_return_id()
    {
        $this->getId()->shouldReturn(USER_ID);
    }

    function it_should_return_username()
    {
        $this->getUsername()->shouldReturn(USER_NAME);
    }
}
