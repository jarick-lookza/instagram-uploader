<?php

namespace spec\Entity;

use PhpSpec\ObjectBehavior;

const FILENAME = 'photo.jpg';
const PATH = '/path/to/photo.jpg';

class FileSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(FILENAME, PATH);
    }
   
    function it_should_return_name()
    {
        $this->getName()->shouldReturn(FILENAME);
    }

    function it_should_return_path()
    {
        $this->getPath()->shouldReturn(PATH);
    }
}
